#
### WHAT?
#

#Purpose
 - Obtain a Wikipedia taxonomy. problems to face in conflicts.
 - Obtain a dendrogram from a clustering proccess over a wikipedia domain pages.
 - Compare the taxonomy and the dendrogram to evaluate by human means the quality of the knowledge representation.
 - Investigate on data knowledge visualization and propose a tool.
 - Try to automate the proccess as much as possible:
    ·Deal with the Wikipedia information.
    ·Build a taxonomy from a Wikipedia domain.
    ·Build a dendrogram from a set of documents.
    ·Compare the taxonomy with the dendrogram.
    ·Generate a classifer from the taxonomy and offer it as a service.
 - Publish the work.
    ·Memory
    ·Paper.
    ·Release of open source libraries.
    ·Text over a domain classification server.


#Why Wikipedia?
 - Huge, collaborative and  increasing.
 - Downloadable.
 - POV.
 - Better set of representation.
 
#Why offline?
 - Speed.
 - Computer Set up.
 
#Custom indexing.




 
#
### CONFLICTS
#


PROBLEM:
    Empty categories (without pages) in the higher levels.
SOLUTION:
    Delete them recursively BFS till finding a category with pages or subcategories with pages.
    
    
PROBLEM:
    The graph is not a Directed Acyclic Graph.
SOLUTION:
    Delete the edge in the cycle with the highest level. It points to a node which appeared prevously an therefore have two levels.
    The graph is always directed, but could contain cycles (extremely unlikely situation).
    Example:
        Root category: Category:Political spectrum
        Depth: 5
        Category:Socialism <-> 'Category:Economic planning'
        
        
PROBLEM:
    There are nodes with multiple parents.
SOLUTION1:
    Perform a transitive reduction. It guarantees reachability and specification.
    Example:
        Root category: Category:Political spectrum
        Depth: 3
        
