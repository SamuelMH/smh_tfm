# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

from pykitools.doc.constants import WIKIFILES, DIR

from pykitools.contents.pages import Pages
from pykitools.contents.categories import Categories
from pykitools.taxonomy.category_graph import CategoryGraph

#from pykitools.taxonomy.retrievers.online import RetrieverOnline
from pykitools.taxonomy.retrievers.offline import RetrieverOffline

from pykitools.taxonomy.conflicts import empty_categories, multiparent_nodes


ROOT_CAT = 'Category:Political spectrum'
#ROOT_CAT = 'Category:Religious faiths, traditions, and movements'
#ROOT_CAT = 'Category:Sports by type'

DEPTH = 3

def save(graph, prefix):
    graph.serialize(to_file='{path}{cat}-{depth}-{prefix}.json'.format(
        path = DIR['vis_wikigraph'],
        prefix = prefix,
        cat = ROOT_CAT,
        depth = DEPTH
    ))

pages = Pages(WIKIFILES['xml_pages'], WIKIFILES['sqlite_pages']) #Load retriever
categories = Categories(pages,WIKIFILES['csv_category'],WIKIFILES['sqlite_categorylinks'])
retriever = RetrieverOffline(categories)

#
### Overview the whole problem
#

# Raw graph
graph = CategoryGraph()
graph.build_dfs(ROOT_CAT,DEPTH,retriever)
save(graph, '1raw')


# Identify conflict nodes
graph.conflict_identify(
    list_func_identify = [
        empty_categories.identify,
        multiparent_nodes.identify
    ]
)
save(graph, '2raw-conflict') #raw graph


# Trim graph to the conflict graph, nodes were previously identified
trim_graph = graph.build_conflict()
save(trim_graph, '3raw-trim')


#
### Conflict solving
#

# Delete empty categories and transitive relationships
trim_graph.conflict_solve(
    list_func_solve= [
        empty_categories.solve,
        multiparent_nodes.solve_transitive
    ]
)
    
# Identify unsolved conflicts
trim_graph.conflict_identify(
    list_func_identify = [
        empty_categories.identify,
        multiparent_nodes.identify
    ]
)
save(trim_graph, '4solve')


#Unsolved graph, the less nodes the better.
unsolved_graph = trim_graph.build_conflict()
save(unsolved_graph, '5solve-trim')


#Unsolved raw graph - The goal is to have here a tree
graph.conflict_solve(
    list_func_solve= [
        empty_categories.solve,
        multiparent_nodes.solve_transitive
    ]
)
graph.conflict_identify(
    list_func_identify = [
        empty_categories.identify,
        multiparent_nodes.identify
    ]
)
save(graph, '6raw-unsolved')
