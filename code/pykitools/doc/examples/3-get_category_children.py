# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

from pykitools.contents.pages import Pages
from pykitools.contents.categories import Categories
from pykitools.doc.constants import WIKIFILES

QUERY = "Category:Anarchism"

pages = Pages(WIKIFILES['xml_pages'], WIKIFILES['sqlite_pages']) #Load retriever
categories = Categories(pages,WIKIFILES['csv_category'],WIKIFILES['sqlite_categorylinks'])
ret_pages,ret_subcats = categories.children(QUERY)
