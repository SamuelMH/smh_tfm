# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

from pykitools.contents.pages import Pages
from pykitools.doc.constants import WIKIFILES

QUERY = "Anarchism"


pages_a = Pages(
    xml_pages=WIKIFILES['xml_pages'],
    sqlite_pages=WIKIFILES['sqlite_pages']
) #Load retriever
p_a = pages_a.get(QUERY) #Retrieve a page
print p_a.keys()

# It is possible to pass a function that proceess the 'raw' field.
from pykitools.utils import WikiExtractor
pages_b = Pages(
    xml_pages=WIKIFILES['xml_pages'],
    sqlite_pages=WIKIFILES['sqlite_pages'],
    f_raw = WikiExtractor.process #Process the page and extract features
)
p_b = pages_b.get(QUERY)
print p_b.keys()
