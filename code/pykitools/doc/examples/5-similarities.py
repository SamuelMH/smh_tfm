# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


#
### Build contents
#
from pykitools.doc.constants import WIKIFILES
from pykitools.contents.pages import Pages
from pykitools.contents.categories import Categories
from pykitools.utils import WikiExtractor

#pages = Pages(WIKIFILES['xml_pages'], WIKIFILES['sqlite_pages'])
pages = Pages(
    xml_pages=WIKIFILES['xml_pages'],
    sqlite_pages=WIKIFILES['sqlite_pages'],
    f_raw = WikiExtractor.process #Process the page and extract features
)
categories = Categories(pages,WIKIFILES['csv_category'],WIKIFILES['sqlite_categorylinks'])


#
### Build graph
#
from pykitools.taxonomy.retrievers.offline import RetrieverOffline
from pykitools.taxonomy.category_graph import CategoryGraph

retriever = RetrieverOffline(categories)
graph = CategoryGraph()
graph.build_dfs('Category:Political spectrum',3,retriever)


#
### Preprocess documents
#
import nltk
from pykitools.doc.constants import TERM_PROCESSING
from pykitools.utils.term_processor import TermProcessor

doc_ids = [id for id,data in graph.nodes(data=True) if data['type']=='page']
term_processor = TermProcessor(
    forbidden_symbols = TERM_PROCESSING['symbols'],
    stopwords = TERM_PROCESSING['stopwords'],
    func_stem = nltk.stem.SnowballStemmer('english').stem
)
docs = []
for doc_id in doc_ids:
    doc = pages.get(doc_id)
    doc['terms'] = term_processor.text2terms(doc['text'])
    del(doc['text'],doc['raw']) #Free unneeded fields
    docs.append(doc)


#    
### Compute similarities
#
from pykitools.similarity.similarity import Similarity
from pykitools.similarity.methods.tfidf import TFIDF
from pykitools.similarity.methods.links_wiki import LinksWiki
from pykitools.similarity.methods.links_shared import LinksShared

sim = Similarity(docs)

## TFIDF
tfidf = TFIDF( #build obj
    args_model = {
        'max_features':1000,
        'use_idf':True,
        'smooth_idf':True,
        'sublinear_tf':True
    }
)
sim.compute_dissimilarity(
    func_dissimilarity = tfidf.compute_dissimilarity,
    func_retrieve = lambda(doc): ' '.join(doc['terms']),
    name='tfidf-cosine'
)
#Export file for cloud of words
#tfidf.d3js


## Wiki links
sim.compute_dissimilarity(
    func_dissimilarity = LinksWiki.compute_dissimilarity,
    func_retrieve = lambda (doc): (doc['title'], doc['links_wiki']),
    name='links_wiki'
)


## Shared links
#sim.compute_dissimilarity(
    #func_dissimilarity = LinksShared.compute_dissimilarity,
    #func_retrieve = lambda (doc): (doc['title'], doc['links_shared']+doc['links_wiki']),
    #name='links_shared'
#)

#
### Clustering
#

