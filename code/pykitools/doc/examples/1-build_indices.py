# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


from pykitools.doc.constants import WIKIFILES
from pykitools.indices import csv_files, sqlite_indices


def process_pages():
    csv_files.pages( #Lasts 25min at 30MB/s ~ 44GB*(1024MB/GB)/(30MB/s)/(60s/min)
        xml_pages= WIKIFILES['xml_pages'],
        csv_out= WIKIFILES['csv_pages']
    )
    sqlite_indices.pages( #Lasts 5min
        csv_pages = WIKIFILES['csv_pages'],
        sqlite_out = WIKIFILES['sqlite_pages']
    )
    #Safe to delete WIKIFILES['csv_pages'] since indexing is done by WIKIFILES['sqlite_pages']


def process_categories():
    csv_files.category( #Lasts 1min
        sql_category = WIKIFILES['sql_category'],
        csv_out = WIKIFILES['csv_category']
    )
    csv_files.categorylinks( #Lasts 15min at 11MB/s ~ 9.5GB*(1024MB/GB)/(30MB/s)/(60s/min)
        sql_categorylinks = WIKIFILES['sql_categorylinks'],    
        csv_category = WIKIFILES['csv_category'],
        csv_out = WIKIFILES['csv_categorylinks'],
    )
    sqlite_indices.categorylinks( #Lasts 17min
        csv_categorylinks=WIKIFILES['csv_categorylinks'],
        sqlite_out=WIKIFILES['sqlite_categorylinks']
    )
    #Safe to delete WIKIFILES['csv_categorylinks'] since indexing is done by WIKIFILES['sqlite_categorylinks']
    #DO NOT delete WIKIFILES['csv_category']


if __name__=='__main__':
    # export PYTHONPATH=$PYTHONPATH:<path to pykitools>
    process_pages()
    process_categories()
