# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


def load_pykitools():     
    try:
        import pykitools
    except:
        import os, sys
        path =  os.getcwd()
        base = '/'.join(path.split('/')[:-3])
        sys.path.append(base)
        try:
            import pykitools 
        except:
            print 'ERROR: pykitools could not be loaded.'
