# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


from itertools import combinations
import pandas as pd


from scipy.cluster.hierarchy import linkage
#http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html


class Similarity():
    
    def __init__(self,documents):
        '''
        Constructor
        
        Parameters
        ----------
        documents: [doc]
            List of documents.        
        '''
        self._documents = documents
        self._documents.sort(key=lambda x:x['id'])
        self._similarities = pd.DataFrame(
            index=[ #possible combinations between documents
                (d1,d2) for d1,d2 
                in (combinations([d['id'] for d in self._documents],2))
            ]
        )
    
    
    def compute_dissimilarity(self, func_dissimilarity, func_retrieve, name):
        '''
        Compute the dissimilarity matrix with a method.
        In a geospatial system it would be the distance.
        
        Parameters
        ----------
        func_dissimilarity: ([document_data]) -> dissimilarity matrix
            Function that takes a list of data (a list per document) and
            returns the pairwise dissimilarity matrix (array).        
        name:
            Name of the dissimilarity method.
        '''
        self._similarities[name] = func_dissimilarity([func_retrieve(x) for x in self._documents])
        return(None)
        
    
    def cluster(self, weights, args_cluster={}):
        '''
        Performs hierarchical clustering.
        http://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        '''
        dis = [self._similarities[c]*w for c,w in weights.items()]
        self._cluster = linkage(dis, **args_cluster)
        
        

        
        
    def get_similarity(self, doc1, doc2, method):
        '''
        Get the precomputed similarity between 2 documents 
        
        Parameters
        ----------
        doc1: int
            Document identifier
        doc2: int
            Document identifier
        method: str
            Method name used to compute the similarity.            
        
        Returns
        -------
        out : float
            Documents similarity computed with the given method
        '''
        return None
        #return(self._similarities[method].get_similarity(doc1,doc2))
        
        
     
    def _index_name(self, doc1, doc2):
        '''
        Since the similarity is a symmetric measure, it will be computed once
        per document pair.
        
        Parameters
        ----------
        doc1: int
            Document identifier
        doc2: int
            Document identifier
        
        Returns
        -------
        out : tuple(doc_id, doc_id)
            The key to access the pair similarity in a dict.
        '''
        retval = (doc1, doc2)
        if doc2<doc1:
            retval = (doc2, doc1)
        return(retval)