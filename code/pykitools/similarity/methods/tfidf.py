# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


from sklearn.feature_extraction.text import TfidfVectorizer
from pykitools.utils.decorators import to_file, to_json

from scipy.spatial.distance import pdist
#http://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html

'''
Computes Term Frequency - Inverse Document Frequency distances between
documents.
http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html
'''
class TFIDF():
    
    def __init__(self, args_model={}):
        self._model = TfidfVectorizer(**args_model)
        self.metric = 'cosine'
    


    def compute_dissimilarity(self, data):
        '''
        Calculate similarity matrix for this method.
        
        Parameters
        ----------
        data: [document]
            Document is a string made of preprocessed (symbols, stopwords, stem)
            terms.
            
        Returns
        -------
        out : matrix
            List of TF-IDF vectors (one per document).
        '''
        self._vectors = self._model.fit_transform(data).toarray()
        return(pdist(self._vectors, metric = self.metric))
    
    
    @to_file
    @to_json
    def d3js(self):
        '''
        Get the term weights in a format suitable for the d3.js library.
        
        Returns
        -------
        out : [ {'key':term, 'value':weight} ]            
            List of dictionaries containing term and TF-IDF weigth decreased 
            ordered by weight. Apropiate representation for d3.js cloud of words
            visualization.
        '''
        n_terms = len(self._vectors)
        retval = [
            {'key':k,'value':v}
            for k,v in
            zip(
                self._model.get_feature_names(),
                sum(self._vectors)/n_terms
            )
        ]
        retval.sort(key=lambda x: x['value'], reverse=True)
        return(retval)
            
        