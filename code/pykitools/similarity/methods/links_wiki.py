# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


from itertools import combinations


class LinksWiki():

    @staticmethod
    def compute_dissimilarity(data):
        retval = []
        for (a_id, a_links),(b_id, b_links) in combinations(data,2):
            retval.append(1.0 / (1 + a_links.count(b_id) + b_links.count(a_id)))
        return(retval)
    