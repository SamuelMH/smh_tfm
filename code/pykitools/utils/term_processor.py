# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


import re


class TermProcessor():
    
    def __init__(self, forbidden_symbols , stopwords, func_stem):
        '''
        Constructor.
    
        Parameters
        ----------
        forbidden_symbols: str
            String with symbols to be replaced by spaces. This strings goes
            inside the class of a regular expression.
        stopwords: [str]
            List with the words not to be saved as terms.
        func_stem: func str->str
            Function used to stem a word.
        '''
        self._forbidden_symbols = forbidden_symbols
        self._stopwords =  stopwords
        self._func_stem = func_stem
    
    
    def text2terms(self,text):
        '''
        Transform a text into a list of terms.
    
        Parameters
        ----------
        text: str
            Text to be processed into a list of terms.
            
        Returns
        -------
        out : [str]
            List of terms.
        '''
        retval = []
        candidates = re.sub(
            u'[{0}]'.format(self._forbidden_symbols), ' ', text
        ).lower().split()
        retval = [
            self._func_stem(term)
            for term in candidates
            if term not in self._stopwords
        ]
        return(retval)