# -*- coding: utf-8 -*-

# Author: Samuel M.H. <samuel.mh@gmail.com>
# Description:
#  Utilities to format Python data structures

from pykitools.utils.decorators import to_file
import numpy as np

 
# Convert data types to string
def _custom_filter(datum):
    if type(datum)==float or type(datum)==np.float32 or type(datum)==np.float64:
        retval = round(datum,3)
        retval = '{0:.3f}'.format(retval)
    elif type(datum)==str:
        retval = datum
        retval = retval.replace('_','\_')        
    else:
        retval = datum
    return(retval)


'''
Description:
    Format an array or a dictionary as a LaTeX table.

Params:
    table: actual data.
    keys:  columns to display in the table. If none, the table will NOT have a header.
    index_key: column to be used as the index.
    row_split: write an horizontal line every row_split lines.
    data_filter: function used to convert a datum to a string.
'''
@to_file
def format_tabular(table, keys=None, columns=None, index_key=None, rows_split=0, data_filter=_custom_filter):
    if not keys:
        keynames = range(len(table[0]))
    else:
        keynames = [data_filter(k) for k in keys]
    if index_key is not None:
        keynames.insert(0,index_key)
    if not columns:
        columns = ' | '.join(['c' for k in keynames])
        if index_key is not None:
            columns = 'c || '+columns
    retval ='\\begin{{tabular}}{{{0}}}\n'.format(columns)
    if keys:        
        retval += ' & '.join(['\\textbf{{{0}}}'.format(k) for k in keynames])
        retval += ' \\\\ \n'
        retval += '  \\hline \n'
        retval += '  \\hline \n'
    i = 1
    for row in table:
        retval += '    '
        retval += ' & '.join(['{0}'.format(data_filter(row[k])) for k in keynames])
        retval += ' \\\\ \n'
        if rows_split>0 and i%rows_split==0:
            retval += '    \\hline\n'
        i+=1
    retval += '\\end{tabular}'
    return(retval)
