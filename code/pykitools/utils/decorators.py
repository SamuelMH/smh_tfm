# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


from json import dumps


def to_file(func):
    '''
    Decorator to write files
    Add 'to_file=path' to the kwargs to write the str return val
    of the decorated function into the path file.
    '''    
    def wrapper(*args, **kwargs):
        try:
            out_file = kwargs.pop('to_file')
        except:
            out_file = None
        retval = func(*args,**kwargs) #Execute!
        if out_file:
            with (open(out_file,'w+')) as fd:
                fd.write(retval)
        return(retval)
    return(wrapper)


def to_json(func):
    '''
    Convert the return value to a json representation.
    Add 'to_json=True' to the kwargs to enable the conversion.
    '''    
    def wrapper(*args, **kwargs):
        try:
            to_json = kwargs.pop('to_json')
        except:
            to_json = False
        retval = func(*args,**kwargs) #Execute!
        if to_json == True:
            retval = dumps(retval)
        return(retval)
    return wrapper