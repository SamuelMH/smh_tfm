

function downloadSVG(button_id, svg_id) {
    svg = d3.select(svg_id+" svg");
    d3.select(button_id)
        .attr("target","_blank")
        .attr("href", "data:image/svg+xml;charset=utf-8;base64," + btoa(
        unescape(
            encodeURIComponent(
                svg.attr("version", "1.1")
                .attr("xmlns", "http://www.w3.org/2000/svg")
                .node().
                parentNode.innerHTML
            )
        )
    ));
}

