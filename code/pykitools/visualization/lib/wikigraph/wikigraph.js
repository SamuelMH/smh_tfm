//Samuel Muñoz Hidalgo (samuel.mh@gmail.com)
//(C) 2014 Samuel M.H. GNU GPL 3.


// Class - Revealing Module Pattern
function WikiGraph(id_svg) {
    
    var id_svg = id_svg;
    
    var node = null,
        link = null,
        force = null;
        
    
    // It's important to have the style in code an not CSS so the SVG export works.
    var style = {
        'svg': {
            'width': 1920,
            'height':1080,
            'margin': '50px',
            'fill': 'gray'
        },
        'layout': {
            'friction': 0.9,
            'linkDistance': 1,
            'charge': -2000            
        },
        'circle': {
            'r': 10,
            'stroke': '#eee',
            'stroke-width': '1px',
            'fill': 'blue',
            'fill-conflict': 'red',
            'opacity': .9
        },
        'rect':{
            'width': 20,
            'height':20,
            'stroke':'#eee',
            'stroke-width': '1px',
            'fill':'green',
            'fill-conflict': 'red',
            'opacity': .9
        },
        'path': {
            'stroke': '#000',
            'fill': '#000',
            'stroke-width': '1px',
            'stroke-opacity': .3
        },
        'text': {
            'fill': '#000',
            'font': '10px sans-serif',
            'opacity': .6
        }
    };  
    

    //graph is a dictionary (json containing the graph data 
    function generate(graph) { 
        //Set canvas
        d3.select("svg").remove();
        var vis = d3.select(id_svg).append("svg");
        vis.attr("width", style['svg']['width'])
            .attr("height", style['svg']['height'])
            .attr("viewport-fill", style['svg']['fill']);
            
        // BACKGROUND FOR ZOOMING & DRAGGING
        vis.append("rect")
            .attr("width", style['svg']['width'])
            .attr("height", style['svg']['height'])
            .attr("opacity", 0)
            .call(graph_zoom);
            
        // CANVAS
        vis.append("g")
            .attr("id","canvas");

            
        vis = d3.select('#canvas');
            
        //LAYOUT
        force = d3.layout.force()
            .nodes(graph.nodes)
            .links(graph.links)
            .linkDistance(style['layout']['linkDistance'])
            .charge(style['layout']['charge'])
            .size([style['svg']['width'], style['svg']['height']])
            .friction(style['layout']['friction'])
            .on("tick",tick)
            .start();
        

            
        // LINKS
        vis.append("defs").selectAll("marker")
                .data(["end"])      // Different link/path types can be defined here
            .enter().append("marker")    // This section adds in the arrows
                .attr("id", "marker-arrow")
                .attr("viewBox", "0 -5 10 10")
                .attr("refX", style['circle']['r']*2)
                .attr("refY", 0)
                .attr("markerWidth",  style['circle']['r'])
                .attr("markerHeight", style['circle']['r'])
                .attr("orient", "auto")
            .append("path")
                .attr("d", "M0,-5L10,0L0,5");
                
        link = vis.selectAll("line")
            .data(force.links())
            .enter().append("path")
                .attr("marker-end", "url(#marker-arrow)")
                .attr('stroke', style['path']['stroke'])
                .attr('fill', style['path']['fill'])
                .attr('stroke-width', style['path']['stroke-width'])
                .attr('stroke-opacity', style['path']['stroke-opacity']); 
            
        //Nodes
        node = vis.selectAll("g")
            .data(force.nodes())
            .enter().append("g") //G is an element container
            .attr("class", function(d) { return d.type; })
            .call(node_drag);
        node.each(
            function(d,i){
                var elem = d3.select(this);
                if (d.type=='page') {
                    elem.append("circle")
                        .attr("class", 'node')
                        .attr("r", style['circle']['r'])
                        .attr("stroke", style['circle']['stroke'])
                        .attr("stroke-width", style['circle']['stroke-width'])
                        .attr("opacity", style['circle']['opacity'])
                        .attr("fill", node_fill);
                } else if (d.type=='category') {
                    elem.append("rect")
                        .attr("class", 'node')
                        .attr("width", style['rect']['width'])
                        .attr("height", style['rect']['height'])
                        .attr("x", -style['rect']['width']/2)
                        .attr("y", -style['rect']['height']/2)
                        .attr("stroke", style['rect']['stroke'])
                        .attr("stroke-width", style['rect']['stroke-width'])
                        .attr("opacity", style['circle']['opacity'])
                        .attr("fill", d.conflict ? style['rect']['fill-conflict'] : style['rect']['fill']);
                }
                //Text
                var title = (d.type=='category')? d.title.slice(9) : d.title;
                elem.append("text")
                    .attr("x", 12)
                    .attr("dy", ".35em")
                    .attr("fill", style['text']['fill'])
                    .attr("font", style['text']['font'])
                    .attr("opacity", style['text']['opacity'])
                    .text(title);
            }
        );
    }   
    
    /*
     * ADD ELEMENTS
     */
    function node_fill(d){
        var color = style['circle']['fill'];
        if (d.conflict){
            color = style['circle']['fill-conflict'];
        }
        return(color);        
    }

    
    /*
     * ANIMATIONS
     */
    function tick() {            
        node.attr("transform", function(d) { //g nodes
            return "translate(" + d.x + "," + d.y + ")";
        });
        link.attr("d", function(d) {  //paths
            return "M" + 
                d.source.x + "," + 
                d.source.y + "A" + 
                0 + "," + 0 + " 0 0,1 " + 
                (d.target.x) + "," + 
                (d.target.y);
        });
    }
    
    
    /*
     * ZOOM
     */
    var graph_zoom = d3.behavior.zoom()
        .on("zoom", zoom);    

    function zoom() {
        d3.select('#canvas').attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }    
         

    
    /*
     *  NODE DRAG
     */
    var node_drag = d3.behavior.drag()
        .on("dragstart", dragstart)
        .on("drag", dragmove)
        .on("dragend", dragend);

    function dragstart(d, i) {
        force.stop() // stops the force auto positioning before you start dragging
    }

    function dragmove(d, i) {
        d.px += d3.event.dx;
        d.py += d3.event.dy;
        d.x += d3.event.dx;
        d.y += d3.event.dy; 
        tick(); // this is the key to make it work together with updating both px,py,x,y on d !
    }

    function dragend(d, i) {
        d.fixed = !d.fixed; // of course set the node to fixed so the force doesn't include the node in its auto positioning stuff
        tick();
        force.resume();
    }
    
    
    /*
     *  EXPORT GRAPH
     */
    function resize() {
        var box = document.getElementById("canvas").getBBox();
        d3.select("#canvas").attr("transform","translate("+(-box.x)+","+(-box.y)+")");
        d3.select(id_svg+" svg")
            .attr("width", box.width)
            .attr("height", box.height);
    }    
    
    function downloadSVG(id_a) {
        resize();
        svg = d3.select(id_svg+" svg");
        d3.select(id_a)
            .attr("target","_blank")
            .attr("href", "data:image/svg+xml;charset=utf-8;base64," + btoa(
            unescape(
                encodeURIComponent(
                    svg.attr("version", "1.1")
                    .attr("xmlns", "http://www.w3.org/2000/svg")
                    .node().
                    parentNode.innerHTML
                )
            )
        ));
    }   
    
    
    return({
        generate: generate,
        downloadSVG: downloadSVG,
        resize: resize  //Debugging purposes and exporting when browser crashes
    });
    
};

