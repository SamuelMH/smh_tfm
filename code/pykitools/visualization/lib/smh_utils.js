//Samuel Muñoz Hidalgo (samuel.mh@gmail.com)
//(C) 2014 Samuel M.H. GNU GPL 3.


// Class - Revealing Module Pattern
var SMHUtils = (function() {
    
        
    //Load a local json file without a server
    function load_local_json_file(id_input, callback){
        var file_selected = document.getElementById(id_input).files[0];
        var reader = new FileReader();
        reader.onload = function(){
            callback(JSON.parse( reader.result ));
        }
        reader.readAsText(file_selected);
    }
      
    return({
        load_local_json_file: load_local_json_file
    });
})();
