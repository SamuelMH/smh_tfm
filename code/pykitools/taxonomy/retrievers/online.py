# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "May-2014"


import urllib2
import json

class RetrieverOnline():

    
    def title2id(self, cat_title):
        return(int(json.load(urllib2.urlopen('http://en.wikipedia.org/w/api.php?format=json&action=query&prop=info&titles={0}'.format(cat_title)))['query']['pages'].keys()[0]))
   
    
    def get_category(self, cat_id):
        try:
            results = json.load(urllib2.urlopen('http://en.wikipedia.org/w/api.php?format=json&action=query&cmpageid={0}&list=categorymembers&cmprop=title|type|ids&cmtype=page|subcat&cmlimit=max'.format(cat_id)))['query']['categorymembers']
            page_ids = [
                (x['pageid'], x['title'])
                for x in results if x['type']=='page'
            ]
            cat_ids = [
                (x['pageid'], x['title'])
                for x in results if x['type']=='subcat'
            ]
        except:
            page_ids,cat_ids = [],[]
        return((page_ids,cat_ids))
    