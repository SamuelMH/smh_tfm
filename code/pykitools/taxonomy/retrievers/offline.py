# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "June-2014"



class RetrieverOffline():
    
    def __init__(self, categories):
        self._categories = categories
        self.title2id = self._categories._pages.title2id
        self.get_category = self._categories.children
    