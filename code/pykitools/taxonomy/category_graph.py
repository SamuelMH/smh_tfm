# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

import json
import networkx as nx
from networkx.readwrite import  json_graph

from pykitools.utils.decorators import to_file, to_json


class CategoryGraph(nx.DiGraph):
            
    
    def add_node(self, node_id, node_attrs, level, parent_id=None):
        '''
        Add a node to the graph.
        
        Parameters
        ----------
        node_id: int
            identifier of the node to add.
        node_attrs: dict
            attibutes of the node to add.
        level:int
            iteration on wich the node was found.
        parent_id: int
            identifier of the parent node.
        '''
        #Add node
        if node_id not in self:
            super(CategoryGraph,self).add_node(node_id,node_attrs)
            self.node[node_id]['levels'] = []
        self.node[node_id]['levels'].append(level)
        #Add parent relationship
        if parent_id:
            self.add_edge(parent_id, node_id,{'level':level})  
        
        
    def build_dfs(self,cat_title, depth, retriever):        
        self._retriever = retriever
        self.not_found = [] #Categories not found
        cat_id = self._retriever.title2id(cat_title)
        #graph properties
        self.graph['root_category'] = cat_title
        self.graph['max_depth'] = depth
        #add_node
        self.add_node(cat_id, {'title':cat_title,'type':'category'},1)
        self._build_dfs(cat_id, depth-1)
    
    
    def _build_dfs(self, parent_cat_id, depth):
        '''
        Depth-First Search category graph construction.
        It does not return anything, just modifies the object.
        
        Parameters
        ----------
        parent_cat_id: int
            identifier of the category to expand.
        depth: int
            iterations remainig.
        '''
        try:
            subpages,subcats = self._retriever.get_category(parent_cat_id)
        except:
            subpages,subcats = [],[]
            self.not_found.append(parent_cat_id)            
        for page_id,page_title in subpages:
            self.add_node(page_id,{'title':page_title, 'type':'page'},
                            self.graph['max_depth']-depth+1 ,parent_cat_id)
        for cat_id,cat_title in subcats:
            self.add_node(cat_id,{'title':cat_title, 'type':'category'},
                            self.graph['max_depth']-depth+1 ,parent_cat_id)
            if depth>1:
                self._build_dfs(cat_id, depth-1)
            
    
    #
    ### SAVE, LOAD, EXPORT
    #
    
    @to_file
    def serialize(self):
        '''
        Get the JSON representation of the graph.        
        '''
        return(json.dumps(json_graph.node_link_data(self)))
            
    
    @classmethod
    def from_file(cls, filename):
        '''
        Build a graph from a file.
        '''
        retval = None
        with (open(filename, 'r')) as fd:
            retval = json_graph.node_link_graph(json.loads(fd.read()))
            retval.__class__ = cls
        return(retval)
    
    
    #
    ###  CONFLICTS
    #
    
    def conflict_identify(self, list_func_identify=lambda x:{}):
        '''
        Set the field 'conflict':True into the conflicting nodes.
        
        Parameters
        ----------
        list_func_identify: [function(graph): {conflict nodes}]
            List of functions that identify conflict nodes.
            If no parameter is given, then all nodes are marked as no conflict.
        '''
        conflicts = set.union(*[f(self) for f in list_func_identify])
        for n in self.nodes():
            self.node[n]['conflict'] = True if n in conflicts else False
    
    
    def conflict_solve(self, list_func_solve):
        for f in list_func_solve:
            f(self)
    
        
    def build_conflict(self):   
        '''
        Generative operation.
        Build a new graph reducing this one to the nodes (and their path to 
        the root) that prevent the graph being a taxonomy tree.
        Nodes have to be marked with the function conflict_identify().
        
        Returns
        -------
        retval: pykitools.taxonomy.CategoryGraph
        '''        
        conflicts = {
            node_id
            for node_id,data in self.nodes(data=True)
            if data['conflict']
        }
        nodes = set()
        for node in conflicts:
            if node not in nodes: #performance
                nodes.add(node)
                nodes.update(nx.ancestors(self,node))
        retval = nx.subgraph(self,nodes)
        retval.__class__ = self.__class__
        return(retval)      
    
    
    #
    ###  UTILITIES
    #
       
    def build_ancestors(self,node):
        '''
        Generative operation.
        Build a new graph from the given node to the root.
        
        Parameters
        ----------
        node: into
            Node id which will be the starting leaf.
        
        Returns
        -------
        retval: pykitools.taxonomy.CategoryGraph
        '''        
        nodes = nx.ancestors(self,node)
        nodes.add(node)
        retval = nx.subgraph(self,nodes)
        retval.__class__ = self.__class__
        return(retval)
        
        
    def search_node(self, title, term_separator=' '):
        '''
        Find nodes by title.
        Criteria for the returned value (the first match is returned):
            1 - Full title matching.
            2 - Names starting with 'title...'
            3 - Name tokenization and pseudo AND matching.
            4 - Name tokenization and OR matching.
        
        Parameters
        ----------
        title: str
            Name of the node you want to find.
        term_separator: str
            Character to split the title by when performing tokenization.
            
        Returns
        -------
        out: full node list
            Nodes matching the criteria.  
        '''
        #Full title search
        retval = [x for x in self.nodes(True) if x[1]['title']==(title)]
        #Begin with search
        if retval == []:
            retval = [
                x for x in self.nodes(True)
                if x[1]['title'].startswith(title)
            ]
        #Pseudo-full term coincidence search        
        terms = '('+ title.replace(term_separator,'|') +')'
        if retval == []: #Term searching
            pattern = re.compile('(^|.+_)'+terms+'(.*_'+terms+'){{{0}}}(_.+|$)'.format(len(terms.split('|'))-1))
            retval = [
                x for x in self.nodes(True)
                if pattern.match(x[1]['title'])
            ]
        #Any term coincidence search
        if retval == []: #Term searching
            pattern = re.compile('(^|.+_)'+terms+'(_.+|$)')
            retval = [
                x for x in self.nodes(True)
                if pattern.match(x[1]['title'])
            ]
        return(retval)