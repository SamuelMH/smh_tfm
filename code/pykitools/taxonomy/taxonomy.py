# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "May-2014"

import networkx as nx
import json
import logging
import re
import pickle

class Taxonomy(nx.Graph):
    
    def __init__(self):
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        super(Taxonomy, self).__init__()
    
    
    @classmethod
    def load(cls, tax_file):
        retval = False
        with open(tax_file,'r') as fd:
            retval = pickle.load(fd)
        return(retval)
    
    
    def save(self, tax_file):
        with open(tax_file,'w+') as fd:
            pickle.dump(self, fd)
        return(None)
            
    
    @classmethod
    def build(cls, cat_title, level_max, retriever):
        tax = cls()        
        logger = tax.logger
        logger.info('BUILDING TAXONOMY TREE FROM THE WIKIPEDIA')
        logger.info('-Info-')
        logger.info(u'\tStarting node (level 0): {0}'.format(cat_title))
        logger.info('\tMaximum subcategory level: {0}'.format(level_max))
        node_id = retriever.title2id(cat_title)
        tax.add_node(node_id,name=cat_title,type='category',level=0)
        tax._expand_tree_bfs([node_id], 1, level_max, retriever)
        tax._clean_tree()
        logger.info('-Summary-')        
        logger.info('\tPages found {0}'.format(
            len([True for n in tax.nodes(True) if n[1]['type']=='page'])
        ))
        logger.info('\tCategories {0}'.format(
            len([True for n in tax.nodes(True) if n[1]['type']!='page']),
        ))
        #Check g is a tree
        assert(nx.is_connected(tax))
        assert(len(tax.nodes()) == len(tax.edges())+1)
        return(tax)
    
    
    def _expand_tree_bfs(self, expand_nodes, level, level_max, retriever):
        '''
        Breadth-First Search node-expansion function. NOT RECURSIVE.
        Stop conditions:
        -The maximum allowed depth (level_max) has been reached.
        -All target pages have been found.
        
        Parameters
        ----------
        expand_nodes: [int]
            Each element is the PageId of a Wikipedia category.
        level: int
            Current depth.
        level_max;
            Maximum allowed depth.
        retriever: 
            
        Returns
        -------
        out: networkx.Graph()
            The modified built graph.    
        '''        
        logger = self.logger
        while(level<=level_max):
            logger.info('Level {0}'.format(level))
            expansion_list = []
            for node_id in expand_nodes:
                logger.debug(u'\t{0}'.format(self.node[node_id]['name']))
                pages, categories = retriever.get_category(node_id)                      
                #Add pages
                logger.debug('\t\tPages:')
                for page_id, page_title in pages:
                    if page_id in self.nodes():                        
                        logger.debug(u'\t\t\t->  Duplicated! {0}'.format(page_title))
                        self.node[page_id]['level'] = level
                        self.remove_edges_from(self.edges(page_id))
                        self.add_edge(node_id, page_id)
                    else:
                        logger.debug(u'\t\t\t{0}'.format(page_title))
                        self.add_node(page_id, name=page_title, level=level ,type='page')                    
                        self.add_edge(node_id, page_id)                        
                #Add categories
                #Don't add the categories in the last execution, they'll have no 
                # pages and will be cleaned by _clean_tree()
                if level<level_max:
                    logger.debug('\t\tSubcategories:')
                    for cat_id, cat_name in categories:
                        logger.debug(u'\t\t\t{0} - {1}'.format(cat_name, cat_id))
                        if cat_id not in self.nodes():
                            self.add_node(cat_id, name=cat_name, level=level, type='category')
                            self.add_edge(node_id, cat_id)
                            expansion_list.append(cat_id)
                        else:
                            logger.debug(u'\t\t\t->  Duplicated! {0} - {1}'.format(cat_name, cat_id))
            expand_nodes = expansion_list
            level += 1
        return(None)
        
    
    def _clean_tree(self):
        '''
        Delete recursivel all the categories with no pages.
        '''
        logger = logging.getLogger(__name__)
        logger.info('CLEANING TREE')
        flag = True
        pages = set([x[0] for x in self.nodes(True) if x[1]['type']=='page'])
        while flag:
            delete_nodes = [
                k for k,v in self.degree().iteritems()
                if v==1 and k not in pages 
            ]
            if delete_nodes==[]:
                flag = False
            else:
                logger.debug('\tDeleting {0} categories'.format(len(delete_nodes)))
                self.remove_nodes_from(delete_nodes)
        return(None)
    

    def find(self, name, term_separator=' '):
        '''
        Find nodes by name.
        Criteria for the returned value (the first match is returned):
            1 - Full name matching.
            2 - Names starting with 'name...'
            3 - Name tokenization and pseudo AND matching.
            4 - Name tokenization and OR matching.
        
        Parameters
        ----------
        name: str
            Name of the node you want to find.
        term_separator: str
            Character to split the name by when performing tokenization.
            
        Returns
        -------
        out: full node list
            Nodes matching the criteria.  
        '''
        #Full name search
        retval = [x for x in self.nodes(True) if x[1]['name']==(name)]
        #Begin with search
        if retval == []:
            retval = [
                x for x in self.nodes(True)
                if x[1]['name'].startswith(name)
            ]
        #Pseudo-full term coincidence search        
        terms = '('+ name.replace(term_separator,'|') +')'
        if retval == []: #Term searching
            pattern = re.compile('(^|.+_)'+terms+'(.*_'+terms+'){{{0}}}(_.+|$)'.format(len(terms.split('|'))-1))
            retval = [
                x for x in self.nodes(True)
                if pattern.match(x[1]['name'])
            ]
        #Any term coincidence search
        if retval == []: #Term searching
            pattern = re.compile('(^|.+_)'+terms+'(_.+|$)')
            retval = [
                x for x in self.nodes(True)
                if pattern.match(x[1]['name'])
            ]
        return(retval)
    
    
    def path_to_root(self, node_id):
        '''
        Return a path from the current node to the root node.
            
        Parameters
        ----------
        node_id: int
            Id of the first node in the path.
            
        Returns
        -------
        out: node id list
            Path from the current node to the root.  
        '''
        current_node = node_id
        retval = [current_node]
        while self.node[current_node]['level']>0:
            candidate_node = min(
                [
                    (e[1], self.node[e[1]]) for e in self.edges(current_node)
                    if self.node[e[1]].has_key('level')
                ],
                key = lambda x: x[1]['level']
            )[0]
            retval.append(candidate_node)
            current_node = candidate_node
        return(retval)


    def mkvisualization(self, json_file, node_id=None):        
        def _expand(node_id):
            retval = {'name': self.node[node_id]['name'].split(':')[1:]}
            categories = [
                n
                for _,n in self.edges(node_id)
                if self.node[n]['type']=='category' and self.node[n]['level']==self.node[node_id]['level']+1
            ]
            if categories:
                categories.sort(key=lambda x: self.node[x]['name'])
                retval['children'] = []
                for c in categories:
                    retval['children'].append(_expand(c))
            pages = [
                {'name':self.node[n]['name']}
                for _,n in self.edges(node_id)
                if self.node[n]['type']=='page'
            ]
            if pages:
                if not retval.has_key('children'):                
                    retval['children'] = []
                pages.sort(key=lambda x: x['name'])
                retval['children'].append({'name': 'pages','children':pages})
            return(retval)
            
        if not node_id:
            node_id = [
                n[0] for n in self.nodes(True)
                if n[1].has_key('level') and n[1]['level']==0
            ][0]
        tree = _expand(node_id)
        with open(json_file, 'w') as fd:
            json.dump(tree, fd)
        return(None)
    
