# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

import networkx as nx #ancestors
from itertools import combinations

def identify(graph):
    '''
    Identify conflicting nodes. Nodes with 2 or more parents.

    Parameters
    ----------
    graph: networkx.DiGraph
    
    Returns
    -------
    out : [int]
        List of node ids, pages and categories with more than 1 parent.
    '''
    return({
        node_id for node_id,degree in graph.in_degree().iteritems()
        if degree>1
    })


def solve_transitive(graph):
    '''
    Performs a transitive reduction over the graph.
    See: 
    - http://en.wikipedia.org/wiki/Transitive_reduction
    - Aho, A. V.; Garey, M. R.; Ullman, J. D. (1972),
    "The transitive reduction of a directed graph"
    Avaliable at: dept-info.labri.fr/~thibault/tmp/0201008.pdf
    NOTE: deleted edges are stored into the 'transitive' list attribute of
    the remaining edge so they can be used to disambiguate in a future.
    

    Parameters
    ----------
    graph: networkx.DiGraph
    '''
    def _remove_relationship(origin, destiny, keep):
        if (origin,destiny) in graph.edges():
            graph.remove_edge(origin, destiny)
        if (keep,destiny) in graph.edges():
            '''
            This relationship could not exist unless parents are reduced in the
            increasing order order of the length of their ancestors.
            i.e. ancestors.sort(key=lambda(_,x):len(x),reverse=False)
            '''
            try:
                graph[keep][destiny]['transitive'].append(origin)
            except:
                graph[keep][destiny]['transitive'] = [origin]
        
    for n in identify(graph):
        ancestors = [(p,nx.ancestors(graph,p)) for p in graph.predecessors(n)]
        for (origin1, ancestors1),(origin2, ancestors2) in combinations(ancestors,2):
            ancestors1.add(origin1)
            ancestors2.add(origin2)
            if ancestors1.issuperset(ancestors2):
                _remove_relationship(origin2, n, origin1)
            elif ancestors1.issubset(ancestors2):
                _remove_relationship(origin1, n, origin2)
    return(None)