# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."


def identify(graph):
    '''
    Identify conflicting nodes. Categories with no children.

    Parameters
    ----------
    graph: networkx.DiGraph
    
    Returns
    -------
    out : [int]
        List category ids.
    '''   
    categories = [
        cat_id for cat_id,data 
        in graph.nodes(data=True) 
        if data['type']=='category'
    ]
    return({
        cat_id for cat_id,out_degree 
        in graph.out_degree(categories).iteritems()
        if out_degree==0
    })


def solve(graph, recursive=True):
    '''
    Remove categories with no succesors.

    Parameters
    ----------
    graph: networkx.DiGraph
        Graph to construct.
    conflict_nodes: [int]
        List of category ids.
    recursive: Boolean
        Repeat the operation until no conflicting nodes are left. EJ: chains.
    '''
    conflict_nodes = identify(graph)
    if conflict_nodes:
        graph.remove_nodes_from(conflict_nodes)
        if recursive:
            solve(
                graph = graph,
                recursive = True
            )
    
