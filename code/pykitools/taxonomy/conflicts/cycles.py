# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

import networkx as nx

def identify(graph):
    '''
    Identify conflicting nodes. Nodes that belong to a cycle.

    Parameters
    ----------
    graph: networkx.DiGraph
    
    Returns
    -------
    out : [int]
        List category ids.
    '''
    retval = []
    if not nx.dag.is_directed_acyclic_graph(graph):
        retval = (set(reduce(lambda x,y: x+y, nx.cycles.simple_cycles(graph))))
    return(retval)


def solve(graph):
    '''
    Solve cycles by removing the link with the highest level.

    Parameters
    ----------
    graph: networkx.DiGraph
        Graph to delete cycles from.
           
    Returns
    -------
    removed_edges: list of 2-tuple (origin,destiny) removed edges.
    '''
    removed_edges = []
    for cycle in nx.cycles.simple_cycles(graph):
        origin,destiny = (
            max(
                [
                    (cycle[x], cycle[(x+1)%len(cycle)])
                    for x in range(len(cycle))
                ],
                key= lambda(orig, dest): graph.edge[orig][dest]['level']
            )
        )
        graph.remove_edge(origin,destiny)
        removed_edges.append( (origin,destiny))
    return(removed_edges)
