# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "May-2014"

import sqlite3
import xml.etree.ElementTree as ET

class Pages():
    
    def __init__(self, xml_pages, sqlite_pages, f_raw=lambda(x):x):
        '''
        Costructor
    
        Parameters
        ----------
        xml_pages: str
            Path to the uncompressed XML file that contains all the pages.
            Download it at http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles.xml.bz2        
        sqlite_pages: str
            Path to the SQLite index file created upon the XML file.
        f_raw: function(str)->dict
            Function that takes the 'raw' text field and returns a dictionary of
            processed fields.
        '''
        self._fd_pages = open(xml_pages,'r')
        self._index = sqlite3.connect(sqlite_pages)
        self._f_raw = f_raw
        
    
    def get(self, query):
        retval = None
        try:
            if type(query)==str:
                offset = self._query_by_title(query)[2]
            else:
                offset = self._query_by_id(query)[2]
            retval = self._fetch_page(offset)
            retval.update(self._f_raw(retval['raw']))
        except:
            pass
        return(retval)
    
    
    def close(self):
        '''
        Close the file descriptors and DB connections.
        '''
        self._fd_pages.close()
        self._index.close()
    
    
    def id2title(self,page_id):
        retval = None
        try:
            retval = self._query_by_id(page_id)[1]
        except:
            print("ERROR (pages.id2title): no id {0}".format(page_id))
        return(retval)
    
    
    def title2id(self,page_title):
        retval = None
        try:
            retval = self._query_by_title(page_title)[0]
        except:
            print("ERROR (pages.title2id): no id {0}".format(page_id))
        return(retval)
              
    
    def _query_by_id(self, page_id):
        return(self._index.execute('Select * from pages where id={0}'.format(page_id)).fetchone())
    
    
    def _query_by_title(self, page_title):
        return(self._index.execute('Select * from pages where title="{0}"'.format(page_title)).fetchone())
    
    
    def _fetch_page(self, offset):
        '''
        Fetch an article.
    
        Parameters
        ----------
        offset: long
            Where to positionate the file pointer so the readed content starts
            with '<page>'.
            
        Returns
        -------
        out : dict
            'id': id of the retrieved article.
            'title': title of the article.
            'text': page, text content of the article in the markdown format.
        '''
        self._fd_pages.seek(offset)
        parser = ET.iterparse(self._fd_pages) #Event is on tag end.
        flag = True
        while flag: #locate element page
            _,elem = parser.next()
            if elem.tag=='page':
                flag = False
        return(
            {
                'id': elem.find('id').text,
                'title': elem.find('title').text,
                'raw': elem.find('revision').find('text').text
            }
        )
    
    