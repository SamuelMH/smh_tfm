# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."

import sqlite3
from pykitools.indices import csv_files

class Categories():
    
    def __init__(self, pages, csv_category, sqlite_categorylinks):
        '''
        Costructor
    
        Parameters
        ----------
        pages: pykitools.contents.pages.Pages object        
        csv_category: str
            path to CSV file containing cat_id, cat_title        
        sqlite_categorylinks: str
            path to sqlite3 file containing category relationships.        
        '''
        self._pages = pages
        self._category = csv_files.load_category(csv_category)
        self._categorylinks = sqlite3.connect(sqlite_categorylinks)
        
    
    def children(self, query):
        '''
        Get the children of a category
    
        Parameters
        ----------
        query: str or int
            Category page_title or category page_id
            
        Returns
        -------
        out : ( [(int,str)*], [(int,str)*] )
            Tuple with a list of child pages and a list of child subcategories.
            Each child page or subcategory is a tuple in the form of 
            (page_id, page_title).        
        '''
        retval = ([],[])
        if type(query)==str:
            cat_title = query
        else:
            cat_title = self._pages.id2title(query)
        if cat_title.startswith('Category:'):
            cat_id = self._category[cat_title[9:].replace(' ','_')]
            children = self._query_children(cat_id)
            retval = (
                [ #pages
                    (page_id,self._pages.id2title(page_id))
                    for page_id,_,page_type in children if page_type==1
                ],
                [ #subcategories
                    (page_id,self._pages.id2title(page_id))
                    for page_id,_,page_type in children if page_type==2
                ]
            )
        return(retval)
    
    
    def _query_children(self, cat_id):
        '''
        Query to the database to obtain the children of a category.
    
        Parameters
        ----------
        cat_id: int
            Identifier of the parent category.
            
        Returns
        -------
        out : [ (int, int, int)* ]
            List of tuples with the children in the form 
            (page_id, cat_id, type of child)       
        '''
        return(
            self._categorylinks.execute(
                'Select * from categorylinks where parent_id={0}'.format(cat_id)
            ).fetchall()
        )