# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "May-2014"


import csv, sqlite3


def pages(csv_pages, sqlite_out):
    '''
    Dump the CSV (id,title,offset) into a SQLite database for fast querying.

    Parameters
    ----------
    csv_pages: str
        Path to the CSV file in the form: page_id,page_title,offset
    sqlite_out: str
        Path to the new SQLite file.
    '''
    con = sqlite3.connect(sqlite_out)
    con.execute("CREATE TABLE pages(id INT, title TEXT, offset INT);")
    with open(csv_pages,'r') as fd:
        for row in csv.reader(fd):
            con.execute(
                "INSERT INTO pages(id,title,offset) VALUES (?,?,?);",
                (int(row[0]),row[1].decode('utf-8'),row[2])
            )
    con.execute("CREATE INDEX i_id ON pages(id);")
    con.execute("CREATE INDEX i_title ON pages(title);")
    

def categorylinks(csv_categorylinks, sqlite_out):
    '''
    Dump the CSV (child,parent,type) into a SQLite database for fast querying.

    Parameters
    ----------
    csv_pages: str
        Path to the CSV file in the form: page_id,page_title,offset
    sqlite_out: str
        Path to the new SQLite file.
    '''
    con = sqlite3.connect(sqlite_out)
    con.execute("CREATE TABLE categorylinks(child_id INT, parent_id INT, type INT);")
    with open(csv_categorylinks,'r') as fd:
        for row in csv.reader(fd):
            con.execute(
                "INSERT INTO categorylinks(child_id,parent_id,type) VALUES (?,?,?);",
                (int(row[0]),int(row[1]),int(row[2]))
            )
    con.execute("CREATE INDEX i_child ON categorylinks(child_id);")
    con.execute("CREATE INDEX i_parent ON categorylinks(parent_id);")