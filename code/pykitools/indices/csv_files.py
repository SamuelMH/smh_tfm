# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "May-2014"


import csv

def pages(xml_pages, csv_out):
    '''
    Create a CSV index file for page fetching.

    Parameters
    ----------
    xml_pages: str
        Path to the XML file that contains all the pages.
        Download it at http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles.xml.bz2
    csv_out: str
        Path to the CSV that will be built.
    '''
    from parser_pages import ParserPages
    parser = ParserPages()
    fd = open(xml_pages,'r')
    fd_csv = open(csv_out,'w+')
    writer = csv.writer(fd_csv)
    parser.ParseFile(
        fd,
        #(id, title, offset)
        lambda (x): writer.writerow((x[0],x[1].encode('utf-8'),x[2]))
    )
    fd_csv.close()
    fd.close()
    
    
def category(sql_category, csv_out):
    '''
    Build a category_id, category_title CSV.
    Doc: http://www.mediawiki.org/wiki/Manual:Category_table

    Parameters
    ----------
    sql_category: str
        Path to the SQL file that contains the categories.
        Download it at: http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-category.sql.gz
    csv_out: str
        Path to the CSV that will be built.
    '''
    fd = open(sql_category, 'r')
    fd_csv = open(csv_out, 'w+')
    writer = csv.writer(fd_csv)
    line = fd.readline()
    while line!='':
        if line.startswith('INSERT INTO `category`'):
            line = line[ line.index('(') : -line[::-1].index(')')] #From '(' to ');\n'
            for cat_id,cat_title,_,_,_ in tuple(eval(line)): #Eval is ugly!! But useful            
                writer.writerow((int(cat_id), cat_title))
        line = fd.readline()
    fd_csv.close()
    fd.close()


def load_category(csv_category):
    '''
    Load into memory a dictionary {category_name: category_id}

    Parameters
    ----------
    csv_category: str
        Path to the category CSV file used to retrieve the category id from its title.        
            
    Returns
    -------
    out : dict {category_name: category_id}
    '''
    retval = {}
    with open(csv_category, 'r') as fd:
        for cat_id, cat_title in csv.reader(fd):
            retval[cat_title] = int(cat_id)
    return(retval)
    
    

def categorylinks(sql_categorylinks, csv_category, csv_out):
    '''
    Build a category_id_child, category_id_parent hierarchy CSV.
    Doc: http://www.mediawiki.org/wiki/Manual:Categorylinks_table

    Parameters
    ----------
    sql_categorylinks: str
        Path to the SQL file that contains the category links.
        Download it at: http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-categorylinks.sql.gz
    csv_category: str
        Path to the category CSV file used to retrieve the category id from its title.
    csv_out: str
        Path to the CSV that will be built.
    '''
    import re
    title2id = load_category(csv_category)
    #Parse SQL file
    fd = open(sql_categorylinks, 'r')
    fd_csv = open(csv_out, 'w+')
    writer = csv.writer(fd_csv)
    pat = re.compile(r"^(?P<cl_from>\d+),'(?P<cl_to>.*)',.*,.*,.*,.*,'(?P<cl_type>\w+)'$",re.DOTALL)    
    TYPES = {'page':1,'subcat':2,'file':3}
    line = fd.readline()
    while line!='':
        if line.startswith('INSERT INTO `categorylinks`'):
            line = line[ line.index('(')+1 : -line[::-1].index(')')-1]
            for elem in line.split('),('):
                try:
                    entry = pat.search(elem).groupdict()
                    elem_type = TYPES[entry['cl_type']]
                    parent_id = title2id[entry['cl_to']]
                    child_id = entry['cl_from']
                    writer.writerow((int(child_id),int(parent_id),int(elem_type)))
                except KeyError:
                    pass
        line = fd.readline()
    fd_csv.close()
    fd.close()
    