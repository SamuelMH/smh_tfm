# -*- coding: utf-8 -*- 
__author__ =  "Samuel Muñoz Hidalgo (samuel.mh@gmail.com)"
__copyright__ = "(C) 2014 Samuel M.H. GNU GPL 3."
__version__ = "May-2014"


import xml.parsers.expat
import sys


class ParserPages():
    
    class Offset():
        '''
        Class used to register the offset in a document providing the pointer
        expat gives will  overflo since XML_LARGE_SIZE option is not enabled in 
        the library.
        See: http://marcomaggi.github.io/docs/expat.html#overview-install
        See: https://docs.python.org/2/library/pyexpat.html
        '''
        
        def __init__(self):
            self._ptr_old = 0 #Last processed pointer returned by expat
            self._offset = 0  #Real offset pointer in the document
            self._SIZE = (sys.maxint+1)*2 #Range of the pointer type returned by expat
        
        
        def calculate(self, ptr_new):
            '''
            Returns the offset in the file fixing the overflow.
            '''
            if self._ptr_old>ptr_new:
                self._offset += (ptr_new+self._SIZE)-self._ptr_old
            else:
                self._offset += ptr_new-self._ptr_old
            self._ptr_old = ptr_new
            return(self._offset)


    def __init__(self):                
        self._index = []
        #Value vars
        self._offset = None        
        self._id = None
        self._title = None
        #Status vars
        self._level = 0
        self._status = None
        
        #
        ### PARSER
        #
        self._parser = xml.parsers.expat.ParserCreate()
        # TUNING
        self._parser.buffer_text = True
        # FUNCTIONS
        self._parser.StartElementHandler = self.StartElementHandler
        self._parser.EndElementHandler = self.EndElementHandler
        self._parser.CharacterDataHandler = self.CharacterDataHandler
        
        #
        ### Overflow fixer
        #
        self._offset_fixer = self.Offset()
    
    
    def StartElementHandler(self, name, attrs):
        self._level +=1       
        if self._level==1:
            if name=='id':
                self._status = 'id'
            elif name=='title':  
                self._status = 'title'
        elif name=='page':
            self._level = 0
            self._offset = self._offset_fixer.calculate(self._parser.CurrentByteIndex)
    
    
    def EndElementHandler(self,name):
        if self._level==0 and name=='page':
            #self._index.append( (self._id,self._title,self._offset) )
            self._func_row( (self._id,self._title,self._offset) )
        self._level -= 1
    
    
    def CharacterDataHandler(self,data):
        if self._status:
            if self._status=='id':
                self._id = int(data)
            elif self._status=='title':
                self._title = data
            self._status = None
    
    
    def ParseFile(self, fd, func_row):
        '''
        Parse the big XML file.

        Parameters
        ----------
        fd: file object
            Opened XML pages file.
        func_row: function(tuple)
            Callback function called when a tuple is built.
        '''
        self._func_row = func_row
        self._parser.ParseFile(fd)
    
    
    #@property
    #def index(self):
        #return(self._index)
    