#
### LIBRARY
#


- How to load the pykitools library:
$ export PYTHONPATH=$PYTHONPATH:<path_to_pykitools_library>





#
### VISUALIZATIONS
#

The visualizations are meant to be seen with a web browser. They are in the 
pykitools/visualization folder and appear as HTML files.

Implemented visualizations:
  - word_cloud.html : a word cloud generator.
  - wikigraph.html : an interactive graph navigator/generator.

To initialize the visualization, open the desired file with a web browser and
load the data which will remain under the pykitools/visualization/data folder.
  
- How to launch chrome to load local files
Append the --allow-file-access-from-files  option.


#
### How to export a huge graph
#

Sometimes when SVG export is pressed, the browser crashes. Follow this steps
to export the graph.
1-Open a Javascript console: Graph.resize()
2-Copy the <svg> HTML element
3-Paste to a file: xclip -o > graph.svg
4-Export to png: rsvg-convert -w 6000 -a graph.svg > graph.png

