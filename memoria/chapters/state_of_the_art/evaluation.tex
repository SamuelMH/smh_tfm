\section{Ontology evaluation}
  Although the terms taxonomy and ontology are indistinctly used, that is a misnomer because ontologies are a generalization of taxonomies. Ontologies label relationships (\emph{is-a} relationships are the only ones in taxonomies) and therefore elements can play different roles; there is no need for a hierarchy between elements so, if a taxonomy can be seen as a ``tree'', an ontology is often more of a ``forest''. An ontology might encompass a number of taxonomies, with each taxonomy organizing a subject in a particular way \cite{tax_ontol_diff_idea}. Ontologies are likely to classify words more carefully, perhaps as parts of speech, which human language, how precisely one word is the exact synonym for another, etc.\\
  
  Here are two more formal definitions.
  \begin{description}
    \item [Taxonomy] \cite{tax_ontol_diff_info} \hfill \\
      A taxonomy is a collection of controlled vocabulary terms organized into a hierarchical structure. Each term in a taxonomy is in one or more parent-child relationships to other terms in the taxonomy. There may be different types of parent-child relationships in a taxonomy (e.g., whole-part, genus-species, type-instance), but good practice limits all parent-child relationships to a single parent to be of the same type. Some taxonomies allow polyhierarchy, which means that a term can have multiple parents. This means that if a term appears in multiple places in a taxonomy, then it is the same term. Specifically, if a term has children in one place in a taxonomy, then it has the same children in every other place where it appears.\\
      A taxonomy has additional meaning specified via whatever the meaning of the hierarchical link is. In a traditional ``taxonomy'' the meaning is generalization/specialization or ``is a kind of'', depending on what direction you are going. These days the word ``taxonomy'' is used to refer to other kinds of hierarchies with different meanings for the links (e.g., part of, broader topic than, instance of). Sloppy taxonomies will not identify explicitly what the meaning of the link is, and there may be different meanings. If a taxonomy has a variety of very carefully defined meanings for the hierarchical link, then it bears a stronger resemblance to an ontology. 
    \item [Ontology] \cite{tax_ontol_diff_info} \hfill \\
      A formal ontology is a controlled vocabulary expressed in an ontology representation language. This language has a grammar for using vocabulary terms to express something meaningful within a specified domain of interest. The grammar contains formal constraints (e.g., specifies what it means to be a well-formed statement, assertion, query, etc.) on how terms in the ontology's controlled vocabulary can be used together.\\ 
      The word ``ontology'', when used in the AI/Knowledge Representation community, tends to refer to things that have a rich and formal logic-based language for specifying meaning of the terms. Both a thesaurus and a taxonomy can be seen as having a simple language that could be given a grammar, although this is not normally done. Usually they are not formal, in the sense that there is no formal semantics given for the language. However, one can create a model in UML and a model in some formal ontology language and they can have identical meaning. It is thus not useful to say one is an ontology and the other is not because one lacks a formal semantics. The truth is there is a fuzzy line connecting these things.      
  \end{description}
  
  The clarification is important because in information science, the study field that deals with knowledge representation, the term ``ontology'' is used rather than ``taxonomy'', and for this reason most of related studies names articles after it.\\
  
  The hard question is: \emph{What is a good ontology?}\\
  Good ontologies are the ones that serve their purpose. Complete ontologies are probably more than what most knowledge services require to function properly. The biggest impediment to ontology use is the cost of building them, and deploying ``scruffy'' ontologies that are cheap to build and easy to maintain might be a more practical and economical option. Equally there has been much focus on the potential of ontology re-use, which would also lower the entry cost. In both cases, the existence of appropriate evaluation methodologies is essential.\cite{ontology_eval_data_driven}\\
  
  Ontology evaluation can be as complex as the taxonomy induction issue, in fact, these two fields are different points of view of the same problem, knowledge representation. Because of that, they share methods that apply slightly differently.
  There are two alternatives when evaluating an ontology, taking it as a whole and compare it against some kind of valid reference (which is the classical approach), or focusing in some parts and testing them independently.\\
  
  Once distinctions have been done, it is time to review the methods used to measure the quality of induced taxonomies, which will the same ones used for ontologies.
  
  
  \subsection{Whole evaluation}
    This set of methods take the ontology as a whole and evaluate it against some kind of authority or purpose. This is the standard approach.
    
    \subsubsection*{Human}
      In this qualitative approach, the evaluation is done by human means, generally by experts in the field who asses how well the ontology meets a set of predefined criteria, standards, requirements, etc. From a human point of view, the quality of the result is the best one of all the methods, at the expense of lacking automatization and consequently beeing unsuitable for arbitrary domains.\\
      The biggest problem here is that it is quite hard to determine who the right users are, and what criteria to propose they use for their evaluation. Should the domain experts be considered the users, or the knowledge engineers, or even the end users? Should they evaluate an ontology more highly because it is ``sensible'', ``coherent'', ``complete'' or ``correct'', and what do we mean by these terms? Furthermore, most users could not evaluate the logical correctness of an ontology.\\
      
    \subsubsection*{Construction criteria}  
      A closely related qualitative approach would be to evaluate an ontology from the perspective of the principles used in its construction. While some of these design principles are valid in theory, it is extremely difficult to construct automated tests which will comparatively evaluate two or more ontologies as to their consistent use of "identity criteria" or their taxonomic rigour. This is because such principles depend on an external semantics to perform that evaluation, which currently only human beings are capable of providing. Furthermore, there is a significant danger that in applying a principles-based approach to ontology construction the result could be vacuous and of no practical use.
      
    \subsubsection*{Golden standard}
      In this case, the authority is a prevously built knowledge base considered a good representation of the concepts in the domain. In the literature, almost always the golden standard is another ontology such as: WordNet\cite{wordnet}, MeSH\cite{mesh} or CyC/OpenCyc\cite{opencyc}.
      
    \subsubsection*{Corpus of documents, data driven}
      This set of methods involve comparisons of the ontology with a source of data (a collection of documents) to measure the congruence between the ontology and a domain of knowledge.\\
      The problem here is that if the results differ from the gold standard, it is hard to determine whether that is because the corpus is inappropriate, the methodology is flawed or there is a real difference in the knowledge present in the corpus and the gold standard. In any case, this approach is more applicable when one is trying to evaluate ontology learning methodologies. In the Semantic Web scenario, it is likely that one has to choose from a range of existing ontologies the most appropriate for a particular domain, or the most appropriate to adapt to the specific needs of the domain/application.\\
      This is an architecture for ontology-corpus evaluation proposed by \cite{ontology_eval_data_driven}:
      \begin{enumerate}
        \item \textbf{Indentifying keywords/terms}\\
          This is essentially a form of automated term recognition, and thus the whole panoply of techniques existing can be applied: TF-IDF, LSA, PLSA \cite{plsi}, etc; and a clustering method.
        \item \textbf{Query expansion}\\
          Because a concept in the ontology is a compact representation of a number of different lexical realisations in a number of ways, it is important to perform some form of query expansion of the concept terms. It can be done using WordNet to add two levels of hypernyms to each term in a cluster. There are other ways to expand the a term using (foexample) IR techniques.
        \item \textbf{Ontology mapping}\\
          Finally, the set of terms identified in the corpus need to be mapped to the ontology.
      \end{enumerate}
      Given a corpus appropriately annotated against an ontology, we could count how many concept terms in the ontology match those lexical items that have been marked up. This would yield initial (crude) measures of lexical keyword coverage by ontology labels (precision and recall). This provides figures which reflect the coverage of the ontology of the corpus. The most common scenario is one where there are items absent as well as items unneeded.\\

      The advantage of using a cluster analysis approach is that it permits the creation of a measure of structural fit. We can imagine two ontologies with identical concept sets which, however, have the concepts differently organised and thus concepts are at a different distance from each other. Thus the authors propose a ‘tennis measure’ \cite{enrich_ontology} for an ontology which evaluates the extent to which items in the same cluster are closer together in the ontology than those in different clusters. What is determined as close is dependent on the probability model used to derive the clusters.\\
      
      The authors express the evaluation of the ``best fit'' between a corpus and one among a set of ontologies as the requirement of finding the conditional probability of the ontologies given the corpus. The ontology that maximizes the conditional probabilityof the ontology $O$ given a corpus $C$ is then the best fit ontology $O^{*}$:
      $$O^{*} = argmax_{O}P(O|C) = argmax_{O}\frac{P(C|O)P(O)}{P(C)}$$   
  
    \subsubsection*{Performance}
      The measure is given the results of using the ontology in an application. From an utility perspective this is the best approach, but the ontology could not have meaning for humans, it could not be a reasonable knowledge representation.
      
  
  
  \subsection{Level-based evaluation}
    An ontology is a fairly complex structure and it is often more practical to focus on the evaluation of different levels of the ontology separately rather than trying to directly evaluate the ontology as a whole. This is particularly true if we want a predominantly automated evaluation rather than entirely carried out by human users/experts. Another reason for the level-based approach is that when automatic learning techniques have been used in the construction of the ontology, the techniques involved are substantially different for the different levels. The individual levels have been defined variously by different authors, but these various definitions tend to be broadly similar and usually involve the following levels \cite{ontology_eval_survey}:
  
    \subsubsection*{Lexical, vocabulary, concept and data level}
      The lexical content of an ontology can be evaluated using an Information Retrieval approach. This approach (applied in \cite{semi_supervised_taxonomies}) use concepts such as precision and recall against a gold standard.\\
      This are the methods:
      \begin{itemize}
        \item \emph{Precision} is the percentage of the ontology lexical entries or concepts that also appear in the golden standard, relative to the total number of ontology words.
        $$Precision = \frac{|O \cap G|}{|O|}$$          
        \item \emph{Lexical Recall} ($LR$) is the percentage of golden standard lexical entries that also appear as concept identifiers in the ontology, relative to the total number of golden standard lexical entries.
        $$Recall = \frac{|O \cap G|}{|G|}$$
      \end{itemize}
      Where:
      \begin{description}
        \item [$O$:] concepts found in the ontology to evaluate.
        \item [$G$:] concepts found in the golden standard.
      \end{description}
       
      A way to achieve a more tolerant matching criteria is to augment each lexical entry with its hypernyms from WordNet or some similar resource. Then, instead of testing for equality of two lexical entries, one can tet for overlap between their corresponding set of words (each set containing an entry with hypernyms)\cite{ontology_eval_survey}.

    \subsubsection*{Hierarchy, taxonomy and other semantic relations}
      This methods compare an automatically generated mapping (i.e. an induced taxonomy) against a gold standard. They focus on the taxonomy edges (i.e. in a Wikipedia taxonomy, \emph{isa} relations between categories).\\
      This will be the nomenclature:
      \begin{description}
        \item [$E_{O}$]: edges of the ontology to evaluate.            
        \item [$E_{G}$:] edges of the golden standard.
      \end{description}
      Metrics:
      \begin{itemize}
        \item \emph{Coverage} measures the size of the intersection between the candidate and the golden standard. It is a precision measure using relations instead of concepts.
        $$Coverage = \frac{|E_{O}\cap E_{G}|}{|E_{G}|}$$            
        \item \emph{Novelty} is the rate of \emph{isa} pairs in the candidate that have no mapping in the golden standard.
        $$Novelty = \frac{|E_{O}-E_{G}|}{|E_{O}|}$$
        \item \emph{Extra Coverage} measures the ``gain'' in knowledge provided by the candidate with respect to the existing bases by calculating the proportion of unmapped category pairs \emph{isa} relation to the total number of semantic relations in the gold standard.
        $$ExtraCoverage = \frac{|E_{O}-E_{G}|}{|E_{G}|}$$
      \end{itemize}
          
      Other similarity measures as seen in \cite{ontology_similarity, fca_applied}.
      \begin{itemize}
        \item \emph{Semantic Cotopy} ($SC$)
        \item \emph{Taxonomy Overlap} ($\bar{TO}$)
        \item \emph{Relation Overlap} ($\bar{RO}$)
        \item \emph{F-Measure}
          $$F = \frac{2*LR*\bar{TO}}{LR+\bar{TO}}$$
      \end{itemize}
      In \cite{ontology_similarity}, the authors propose several measures for comparing the relational aspects of two ontologies. Although the need for a golden standard when evaluating an ontology is in a way a drawback, an important positive aspect is that once defined, comparison of two ontologies can proceed entirely automatically.
    
    \subsubsection*{Context level}
      Sometimes the ontology is a part of a larger collection of ontologies that may reference one another. This context can be used for evaluation of an ontology in various ways.\\
      It is possible to use cross-references between semantic-web documents to define a graph and then compute a score for each ontology using for example the PageRank algorithm \cite{pagerank}. As relationships between ontologies can be labeled different, there will be several networks and therefore several ontology rankings to choose depending on the needs.    
    
    \subsubsection*{Application level}
      This is the same as the performance approach in the whole level. A good ontology is one which helps the application in question produce good results on the given task. This is elegant in the sense that the output of the application might be something for which a relatively straightforward and non-problematic evaluation approach already exists.\\
      This evaluation method has several drawbacks:
      \begin{itemize}
        \item An ontology is good or bad when used in a particular way for a particular task, not by itself alone.
        \item Then effect of the ontology in the application could be small or indirect, so the outcome is not meaningful.
        \item Comparing different ontologies is only possible if they can all be plugged into the same application.
      \end{itemize}      
   
    \subsubsection*{Syntactic level}
      This is mostly used for manually constructed ontologies. The ontology is usually described in a particular formal language and must match the syntactic requirements of that language. Various other syntactic considerations, such as the presence of natural-language documentation, avoiding loops between considerations, etc., may also be considered.
        
    \subsubsection*{Structure, architecture, design}
      This  is primarily of interest in manually constructed ontologies. The ontology must meet certain predefined design principles or criteria; structural concerns involve the organization of the ontology and its suitability for further development.
      
    \subsubsection{Multi-criteria approaches}
      Another family of approaches to ontology evaluation deals with the problem of selecting a good ontology (or a small short-list of promising ontologies) from a given set of ontologies, and treats this problem as essentially a decision-making problem. To help us evaluate the ontologies, we can use approaches based on defining several decision criteria or attributes; for each criterion, the ontology is evaluated and given a numerical score. An overall score for the ontology is then computed as a weighted sum of its per-criterion scores. A drawback is that a lot of manual involvement by human experts may be needed. In effect, the general problem of ontology evaluation has been deferred or relegated to the question of how to evaluate the ontology with respect to the individual evaluation criteria. On the positive side, these approaches allow us to combine criteria from most of the levels.\cite{ontology_eval_survey}
   
    
  \subsection{Conclusions}
    \begin{smhEmph}
      Ontology evaluation remains an important open problem in the area of ontology-supported computing and the semantic web. There is no single best or preferred approach to ontology evaluation; instead, the choice of a suitable approach must depend on the purpose of evaluation, the application in which the ontology is to be used, and on what aspect of the ontology we are trying to evaluate. In our opinion, future work in this area should focus particularly on automated ontology evaluation, which is a necessary precondition for the healthy development of automated ontology processing techniques for a number of problems, such as ontology learning, population, mediation, matching, and so on.\cite{ontology_eval_survey} 
    \end{smhEmph}
    