\section{The Wikipedia encyclopedia}
  Wikipedia is a free-access, free content Internet encyclopedia, supported and hosted by the non-profit Wikimedia foundation. Almost anyone who can access the site can edit almost any of its articles. Wikipedia is the sixth-most popular website and constitutes the Internet's largest and most popular general reference work.\cite{def_wiki_wikipedia}.\\
  
  Wikipedia facts \cite{wiki_size}:
  \begin{itemize}
    \item There are currently 33,780,664 articles in the Wikipedia; 4,599,383 of them in the english version.
    \item The figure \ref{wiki_size_volumes} tries to illustrate how big the English-language Wikipedia might be if the articles (without images and other multimedia content) were to be printed and bound in book form. Each volume is assumed to be 25 cm tall, 5 cm thick, and containing 1,600,000 words or 8,000,000 characters. The size of this illustration is based upon the live article count.
    \item The figure \ref{wiki_size_gompertz} shows the number of articles of the english wikipedia (thick blue line) compared with a Gompertz model that leads eventually to a maximum of about 4.4 million articles (thin green line).
    \item The figure \ref{wiki_size_lang_articles} compares the growth of the ten largest Wikipedias. The sum includes all 270+ Wikipedia languages.
  \end{itemize}
  
  \smhImg[15cm]{Estimated size (August 2010) of the printed english Wikipedia, 2036 volumes and 11 stacks.}{wiki_size/volumes.pdf}{wiki_size_volumes}
  \smhImg{Actual articles (blue) versus Gompertz model (green).}{wiki_size/gompertz.png}{wiki_size_gompertz}
  \smhImg{Top 10 Wikipedias comparison.}{wiki_size/lang_articles.png}{wiki_size_lang_articles}
  
  \subsection{Dataset}  
    Wikipedia has only existed since 2001 and has been considered a reliable source of information for an even shorter amount of time, researchers in NLP have just recently begun to work with its content or use it as a resource. Wikipedia has been successfully used for a multitude of AI and NLP applications. These include both preprocessing tasks such as named entity and word sense disambiguation, text categorization, computing semantic similarity of texts, coreference resolution and keyword extraction, as well as full-fledged, end-user applications such as question answering, topic-driven multi-document summarization, text generation and cross-lingual information retrieval.\cite{taxonomy_induction}\\
    
    This are the main points for taking the Wikipedia as dataset when facing AI problems:
    \begin{itemize}
      \item It is a huge encyclopedia, and due its collaborative nature, the amount of pages increases everyday.
      \item It is up to date.
      \item While the contribution of any individual user might be imprecise or inaccurate, the continual intervention of expert contributors in all domains results in a resource of the highest quality \cite{giles2005}.
      \item Provides an acceptable coverage on most popular domains.
      \item It is downloadable and free to use \cite{wiki_copyrights}.
      \item Articles are written under the Neutral Point Of View criteria, which means representing fairly, proportionately, and, as far as possible, without bias, all of the significant views that have been published by reliable sources on a topic \cite{wiki_npov}.
    \end{itemize}
    
    Because of this, Wikipedia is a good representation of the current human knowledge.\\
    
    From a computational linguistics perspective, knowledge bases for NLP applications should be:
    \begin{itemize}
      \item Domain independent.
      \item Up-to-date.
      \item Multilingual.
    \end{itemize}
    
    Wikipedia fulfils all this requirements.
    
    
  \subsection{Categorization}
    This subsection has been greatly inspired by the article:\emph{Tagging wikipedia: Collaboratively creating a category system} \cite{tagging_wikipedia}.\\
    
    Although Wikipedia started as a set of articles, two years after the beginning, the community decided to create a category system to organize and tag the content of the site. This is done by assigning articles to categories through links in a similar way to the metadata practice of adding a tag to a piece of content.\\
    
    In the field of information science, \textbf{knowledge organization (KO}) theorizes, analyzes and critiques systems designed to organize information. From a KO perspective, the Wikipedia category system can be seen as a thesaurus built through collaborative tagging. Hierarchies appear when adding categories to other categories, and as a result, an implicit concept network raises.\\
    
    \subsubsection{Relationships}
      This are the relationships that can be found in a taxonomy \cite{tax_glossary}.
      \begin{itemize}
        \item \textbf{Associative}: a relationship between or among terms that leads from one term to other terms that are related to or associated with it. An associative relationship is a related term or cross-reference relationship.
        \item \textbf{Equivalence}: a relationship between or among terms in a controlled vocabulary that leads to one or more terms that are to be used instead of the term from which the reference is made. An equivalence relationship is a \emph{used-for} term relationship.
        \item \textbf{Hierarchical}: a relationship between or among terms in a controlled vocabulary that depicts broader (generic) to narrower (specific) or whole-part relationships. A hierarchical relationship is a broader term to narrower term relationship.
        To understand the use of this hierarchical structure, it is mandatory to know the types of hierarchical relationships \cite{thesaurus_construction}.
          \begin{itemize}
            \item \textbf{Generic}: a generic hierarchical relationship is defined as a conceptual transitive closure. There are very few examples of this in the category system. But there are other indexing tools in Wikipedia that are organized in this way, for example, the Wikipedia page for ``List of birds''. That is, if we take the class to be ``birds'' all of the pages for which links are supplied in the list are pages for birds.
            \item \textbf{Whole-part}: this relationship consists of a single concept or entity as the class with parts of that concept or entity as the subclass. An example of this type of hierarchical relationship in Wikipedia would be the pages listed under the category ``States of the United States''. Other than the page ``U.S. state'', the other pages under this category are all parts of the category itself.
            \item \textbf{Instance}: these are general concepts or classes which have specific instantiations as a subclass. It is difficult to find examples of categories for which the subcategories are all instances of the category. This is more often achieved through lists in Wikipedia. An example of this type of hierarchical relationship in Wikipedia would be the ``List of cathedrals'' page. If we take ``cathedrals'' to be the class, all of the cathedrals listed on that page are instances of the class.
            \item \textbf{Polyhierarchial}: this type of relationship describes cases when one term is located underneath more than one category. Many categories in Wikipedia are located in more than one parent category. Polyhierarchy is very common in the category system of Wikipedia.
          \end{itemize}
      \end{itemize}
    
    All four types of hierarchy relationships are in use in the category system of Wikipedia.
    
    \subsubsection{Purpose}
      The fact that the relationships between supercategories and subcategories in Wikipedia include both hierarchical relationships as well as associative relationships is due to the fact that the category system emerged from a community in which there were divergent views of what the system should look like.
      
      \begin{itemize}
        \item One of the editors who contributed to the discussions in the dataset stated the need for hierarchical and associative relationships.
        \begin{smhTexttt}
          So I think we need a way of distinguishing between a category where (a) you are asserting that everything in the category is an example of the thing it is in (ie list categories), and (b) categories where you are just providing hierarchical links for convenience.
        \end{smhTexttt}        
        \item A second editor said that a page might need multiple category designations. A clear articulation of the need for polyhierarchy.
        \begin{smhTexttt}
          I’m thinking about some of the dog topics. For example, dog is a member of pets; dog is also a member of mammals; both mammals and pets are members of animals but neither is a subcategory of the other. Now, how about dog agility? It needs to go under the dog sports category, which needs to be under the dog category, because it's related to dogs. It also needs to go under the sports category, because it's a sport. It probably also needs to go under the hobby category. But dog and sports do not at any higher point in the hierarchy have a common parent.
        \end{smhTexttt}        
        \item Other editors felt that the work of scoping the category system of Wikipedia was such a large task that it should be modeled on existing structures for information organization. One editor brought up the challenge of making relationship types explicit and suggested modeling the category system on the Resource Description Framework (RDF).
        \begin{smhTexttt}
          The fix is to label the arrows: describe the  relations. This  is, in my limited understanding, what RDF does. That uses the  terms subject, predicate, and object. The subject is the thing you're categorizing. The object is the category you're adding it to.  And  the  predicate  describes  the relation. Predicates allow you to make semantic  inferences  programmatically.
        \end{smhTexttt}
      \end{itemize}
      
      Some members of the community felt that the amount of effort that was being expended in the design of the category system could be reduced if the purpose of the category system were explicitly articulated.
      \begin{smhTexttt}
        A question concerning the purpose of categories: Is the primary purpose of categories to: Aid the reader in finding material that may be of interest, or relevant to a particular topic? Producing a taxonomy; wherein being included in one or more categories is an indication-nay, a declaration by the Wikipedia community-that the subject of an article is an instance of the category it is included in. I seem to suspect the latter\dots
      \end{smhTexttt}
      \begin{smhTexttt}
        There seems to be a dichotomy between those who are looking to hone categories into encyclopedic taxonomies and those who are looking for a tagging system in which they can do keyword searches. The more we push at removing overcategorization, the more there is a need for a simpler tagging system. If we can answer that need, it might make everyone happier.
      \end{smhTexttt}
    
    \subsection{Problems}
      The lack of consensus with the purpose of the categorization system can be summarized in two points of view.
      On one hand, there is the \textbf{searching purpose}, that is, the user starts navigating at a top category and gets down in the hierarchy to the desired article. This is the most taxonomy-related approach of the structurey.
      On the other hand, there is the \textbf{browsing purpose}. The structure is used as a search-for-related-things tool. They navigate through the structure aiming to find related content, but the aspects of the relationships can vary. This view is related to the polyhierarchial relationships and will be the problem when converting a concept network into a taxonomy.\\
      
      Again, the view of categories as a taxonomy could be against the NPOV policy and, therefore destined to fail \cite{tagging_wikipedia}. This is a policy the editors take seriously.\\
      
      Other underrated problem is the need for visualization tools. While Wikipedia has some extensions that allow for the exploration of the category system, there is nothing that specifically visualizes the assumed or real relationships among category nodes. A tool that in some way displayed the relation among categories would help regular users navigate with the category system and help individuals who wanted to tag pages \cite{tagging_wikipedia}.
