\section{Taxonomy induction}
  Recalling the problem of how to organize the gathered knowledge when there is no initial taxonomy, the automatic methods meant to build a taxonomy from scratch share two common stages:
  \begin{enumerate}
    \item \textbf{Term an relation extraction.} The produced output typically contains flat lists of terms and general relation types (term1 \emph{is-a} term2). There are two approaches:
      \begin{itemize}
        \item \textbf{Clustering} approaches are fully unsupervised and discover relations that are not directly expreseed in text. Their main drawback is that the may or may not produce the term types and granularities useful for the user.
        \item \textbf{Pattern bassed} approaches harvest information with high accuracy, but they require a set of seeds and surface patterns to initiate the learning process.
      \end{itemize}      
    \item \textbf{Taxonomy induction}. Consists on setting the relations between terms so the induced structure is a taxonomy.
  \end{enumerate}
  
  Taxonomizing the terms is a very powerful method to leverage added information. Subordinated terms (hyponyms) inherit information from their superordinates (hypernyms), making it unnecessary to learn all relevant information over and over for every term in the language. But despite many attempts, no ``correct'' taxonomization has ever been constructed. Typically, people build term taxonomies (and/or richer structures like ontologies) for particular purposes, using specific taxonomization criteria. Different tasks and criteria produce different taxonomies, even when using the same basic level concepts. This is because most basic level concepts admit \textbf{multiple perspectives}, while each task focuses on one, or at most two, perspectives at a time \cite{semi_supervised_taxonomies}. This is a manifestation of the polyhierarchial relationship.
  
  Attempts at producing a single multi-perspective taxonomy fail due to the complexity of interaction among perspectives, and people are notoriously bad at constructing taxonomies adherent to a single perspective when given terms from multiple perspectives. This issue and the major alternative principles for taxonomization are discussed in \cite{hovy2002}.\\

  According to the the source of data, there are two alternatives to generate a taxonomy by automatic means.
  
  
  \subsection{Previous structure}
    The starting point is a concept network structure (i.e. the Wikipedia categorization system) and the aim is to expand it with new concepts or to adjust the relations to get a tree-like structure, a taxonomy. The benefit of this alternative is that there is no need to perform the term and relation extraction step, so the effort will be on the taxonomy induction step by refining relations. This are the two main approaches.
  
      \subsubsection{Semantic based}
        This approach tries to mimic the human-decision-taking process by filtering concepts and relationships with regular expressions. This is the most common and best performing way to induce taxonomies, but since they are language dependant and, therefore, require a set of rules based on the vocabulary as well as the gramatic, they cannot be considerated a fully automated method but a semi-supervised one. Great work has been done in the English language. According to \cite{deriving_tax_wikipedia, taxonomy_induction} the main methods of this approach are (note the order importance of the methods):
        \begin{enumerate}
          \item \textbf{Category network cleanup} \label{semantic_cleanup}\\
            This preprocessing step cleans the network from meta-categories by removing all nodes whose labels contain any of the following strings: \texttt{wikipedia, wikiprojects, lists, mediawiki, template, user, portal, categories, articles, pages}.
          \item \textbf{Refinement link identification} \\
            This preprocessing step identify \emph{is-refined-by} relationships. It can be done in two ways:
            \begin{itemize}
              \item Taking all categories containing ``\texttt{by}'' in the name and label all links with their subcategories.
              \item Identify category pairs that match whith the patterns \texttt{X Y} and \texttt{X by Z} (i.e. Miles Davis Albums and Albums by Artist).
            \end{itemize}
          \item \textbf{Syntax-based methods} \\
            The first set of processing methods to label relations between categoris as \emph{isa} is based on string matching of syntactic components of the category labels.
            \begin{enumerate}
              \item \textbf{Head matching} \\
              Given the lexical heads for a pair of categories, a category link \emph{isa} is stablished if the two categories share the same head lemma, as determined by a finite-state morphological analyzer (i.e. \texttt{British Computer Scientists} \emph{isa} \texttt{Computer Scientists}).
              \item \textbf{Modifier matching} \\
              A link between categories is labeled as a \emph{notisa} relationship if the stem of the lexical head of one of the categories occurs in a modifier position in the other catgory (i.e. \texttt{Crime Comics} \emph{notisa} \texttt{Crime}).
            \end{enumerate}
          \item \textbf{Connectivity-based methods} \\
            The connectivity-based methods utilizes the structure and connectivity of the categorization network to provide \emph{isa} links in cases where relations are unlikely to be found in free text.
            \begin{enumerate}
              \item \textbf{Instance categorization} \\
                This method labels \emph{instance-of} relationships heuristically by determining wether the head of the page category is plural.
            \end{enumerate}
          \item \textbf{Lexico-syntactic based methods} \\
            A majority voting strategy is used on pattern matching to find \emph{isa} and \emph{notisa} relationships. This patterns are highy language dependant, see \cite{taxonomy_induction}.
          \item \textbf{Inference-based methods} \\
            The last set of methods propagates the previously found relations by means of multiple inheritance and transitivity.
            \begin{enumerate}
              \item \textbf{Multiple inheritance propagation} \\
                First, propagate all \emph{isa} relations to those super-categories whose head lemmas match the head lemma of a previously identified \emph{isa} super-category.
              \item \textbf{Transitivity propagation} \\
                Second, propagate all \emph{isa} links to those super-categories which are connected through a path found along the previously discovered subsumption hierarchy, thus taking the transitive closure of the \emph{isa} relation.
            \end{enumerate}
        \end{enumerate}

      \subsubsection{Graph based}
        This approach uses the structure of the concept network rather than the node names to set relations.\\
        \begin{enumerate}
          \item \textbf{Cycle removal}\\
            This method deletes al the cycles in the graph.
          \item \textbf{Transitive Reduction} \cite{transitive_reduction} \label{transitive_graph}\\
            The transitive reduction of a directed graph is a graph with as few edges as possible that has the same reachability relation as the given graph.\\
            These are some interesting properties:
            \begin{itemize}
              \item The transitive reduction of a graph is unique.
              \item The method removes redundant relationships that can be infered by the transitive property.
            \end{itemize}
          \item \textbf{Longest path} \cite{semi_supervised_taxonomies} \label{longest_path}\\
            This method surmount the problem in which multiple paths can be taken to reach from one concept to another. Intuitively, finding the longest path is equivalent to finding the taxonomic organization of all concepts.
        \end{enumerate}
        
      \subsubsection{Hybrid}
        This methods mix the two previous approaches.
        \begin{enumerate}
          \item \textbf{Redundant categorization} \cite{taxonomy_induction}\\
            This method tags the relations between two directly connected categories as \emph{isa} if there is at least one page categorized in both categories and the category labels both have a plural head .
        \end{enumerate}


  
  
  \subsection{Corpus of documents}
    This alternative takes a corpus of documents under the taxonomy domain and uses their content to extract concepts, relationships and induce a taxonomy or enrich a previous one.\\
    According to the document interpretation, two approaches can be taken.
    
    \subsubsection{Similarity based}
      This methods are characterized by the use of a similarity/distance measure to compute the pairwise similarity/distance between two documents, represented as word or term vectors, in order to decide if they can be clustered together or not.\\
      The common steps they share are:
        \begin{enumerate}
          \item The construction of a \emph{set of terms}, usually called Bag of Words or BOW. The words are extracted from the documents and mostly ever are: nouns, verbs, adverbs and adjectives. Then they are stemmed so that the lexical root is taken as the term, this way the generalization power is guaranteed.
          \item The \emph{term weighting}, usuallly done with the Term Frequency-Inverse Document Frequency (TFIDF) \cite{tfidf} algorithm. At this point there is a document-term matrix, a vector space model \cite{vector_space_model} representing the documents as a set of weightened terms.
          \item The \emph{clustering of terms}. A process where is decided which terms belongs to the same group owing to the similarity between them.
        \end{enumerate}
      An example of this technique can be found in \cite{wiki_entries_to_wordnet_synsets}, where the authors map Wikipedia articles to WordNet synsets. They compare the results obtained by stemming or not the terms and using the dot product versus the cosine as the similarity measure in the vector space model.\\
      The BOW approach is not meant to induce a taxonomy from scratch, as a notorious disadvantage of this model is that it ignores the semantic relationship among words.\\
      
      The size of the bag of words is frequently huge, up to thousands of terms, the easiest and mostly accepted way to deal with this manifestation of the \emph{curse of dimensionality} is selecting only the top \emph{k-terms}. Nevertheless there are other methods to reduce the dimensionality \cite{evaluation_feature_selection}.
      \begin{itemize}
        \item \textbf{Feature extraction}\\
          It is a process that extracts a set of new features from the original features through some functional mapping such as principal component analysis (PCA) and word clustering. The downside is that the generated new features may not have a clear physical meaning so the clustering results are difficult to interpret.
        \item \textbf{Feature selection}\\
          It is a process that chooses a subset of terms from the original feature set according to some criterion. The selected feature retains the original physical meaning and provides a better understanding for the data and learning process. Depending on if the class label information is required, feature selection can be either unsupervised or supervised.\\
          This is a small summary covering some of this criteria \cite{evaluation_feature_selection}:
          \begin{itemize}
            \item \textbf{Information Gain}\\
              A supervised method that measures the number of bits of information obtained for category prediction by the presence or absence of the term in a document.
            \item \textbf{ $\chi^{2}$ statistic (CHI)}\\
              A supervised method that measures the association between the term and the category.
            \item \textbf{Document Frequency}\\
              An unsupervised, simple but flexible, method that counts the number of documents in which a term occurs. It scales linerarly to large datasets.
            \item \textbf{Term strength}\\
              It is computed based on the conditional probability that a term occurs in the second half of a pair of related documents given that it occurs in the first half. Since it is necesary to calculate the similarity for each document pair, the time complexity is quadratic to the number of documents. It is an unsupervised method.
            \item \textbf{Entropy-based Ranking}\\
              The term is weightened by the entropy reduction when it is removed. The most serious problem of this method is its high computation complexity $O(MN^{2})$. It is impractical when there is a large number of documents and terms, and therefore, sampling technique is used in real experiments.
            \item \textbf{Term Contribution}\cite{evaluation_feature_selection}\\
              The document frequency method assumes that each term is of same importance in different documents, it is easily biased those common terms which have high document frequency but uniform distribution over different classes. Term contribution is proposed to deal with this problem.
          \end{itemize}          
      \end{itemize}
      
      A remarkable work comparing different representations (TF-IDF, Latent Semantic Indexing and multi-word) for documents can be seen at \cite{comparative_tfidf_lsi_multiword}.       
      
    \subsubsection{Graph based}
      A corpus of documents can also be clustered by the analysis of the network that, through hyperlinks, connect them. This are the most relevant algorithms:
      \begin{enumerate}
        \item \textbf{Edge betweenness and clustering coefficient}\\
          The assumption is that edges lying in most of the shortest paths in the graph or with low clustering coefficient are likely to connect separate communities. By recursively deleting the edges with larger betweenness or low clustering, the graph splits into its communities.
        \item \textbf{Network modularity optimization}\\
          They form cluster of nodes so that the density of link within the communities are maximized against the number of links among communities.
        \item \textbf{Spectral methods}\\
          They are based on the analysis of the eigenvalues and eigenvectors of suitably chosen functions of adjacency matrix.          
        \item \textbf{MLC algorithm}\cite{taxonomy_clustering_mlc}\\
          It detects strongly interconnected communities of nodes in a network by finding the attraction basins of random walks on the graph. It provides a fast response in a reasonable time even for networks including thousands of nodes, and can be tuned opportunely in order to maintain its efficiency.          
      \end{enumerate}
      Unfortunately, all the methods except the last one can only be applied in small graphs as they turn unusable in larger networks since they require exceeding computational resources of time. Even the MLC algorithm is unable to cluster large Wikigraphs.\\
      The conclussion of \cite{taxonomy_clustering_mlc} have great implications on considering links as a valid similarity measure.\\
      \label{clustering_problem}
      \begin{smhTexttt}
       The varying agreement between clustering and categorization across the studied versions of Wikipedia suggests that links in Wikipedia do not necessarily imply similarity or relatedness relations. From a technological point of view, this observation implies that, before switching to automatic categorization of items in Wikipedia and in other information networks, it should be tested how the selected clustering algorithm performs with respect to manual indexing.
      \end{smhTexttt}

        
    \subsubsection{Set theoretical}
      This approaches partially order the objects acording to the inclusion relations between their attribute sets. The most commonly used method is the Formal Concept Analysis or FCA \cite{fca_linguistic}, that relies on lattices to represent data. Works ave been done in taxonomy induction \cite{fca_set} and concept clusterization \cite{fca_applied}.


