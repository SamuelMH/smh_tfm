\section{File layer}
  This layer contains the files needed by the framework. This files are the Wikipedia dataset and an index structure over it to guarantee a fast information retrieval.

  \subsection{The Wikipedia dataset}
    In the previous chapter, the goodness of the Wikipedia encyclopedia as a good dataset was justified. 
    
    \subsubsection{Working offline}
      There are two options when accessing the Wikipedia contents. The first one is on-line through the API \cite{wiki_api}. Even though this is the easiest method, it is unsuitable for heavy-duty tasks unless we want to collapse the non-profit Wikimedia Foundation servers. On the other hand, there is the off-line option. It is possible to download all the contents in a few files and manage them on our own. This methodology have several benefits:
      \begin{itemize}
        \item Relieve the Wikipedia servers from intensive querying.
        \item Speed up your queries .
        \item Have an inmutable dataset to compare results.
        \item Independence from your network connection.
      \end{itemize}
      \paragraph{\hfill}\hfill\\
      These advantages are provided at the following costs:
      \begin{itemize}
        \item Downloading the Wikipedia dump can be slow due to the size of the files.
        \item Building the indices takes time, it is a slow process.
        \item It is necesary to store all the data in your machine.
      \end{itemize}

      Fortunately, the first two actions are only executed once (at the start of the project) while for the third drawback, nowadays data storage has become extremely cheap.
      
      
    \subsubsection{Files}
      This are the files to download:
      \begin{itemize}
        \item \textbf{Pages} \\
          XML file that contains all the pages.
          \begin{itemize}
           \item \texttt{URL:} \url{http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles.xml.bz2}
           \item \texttt{Structure:} \url{http://www.mediawiki.org/xml/export-0.6.xsd}
          \end{itemize}
          
        \item \textbf{Categories} \\
          SQL file that contains the category names and identifiers.
          \begin{itemize}
           \item \texttt{URL:} \url{http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-category.sql.gz}
           \item \texttt{Structure:} \url{http://www.mediawiki.org/wiki/Manual:Category_table}
          \end{itemize}
          
        \item \textbf{Categorylinks} \\
          SQL file that contains the association between categories and pages or other categories.
          \begin{itemize}
           \item \texttt{URL:} \url{http://dumps.wikimedia.org/enwiki/latest/enwiki-latest-categorylinks.sql.gz}
           \item \texttt{Structure:} \url{http://www.mediawiki.org/wiki/Manual:Categorylinks_table}
          \end{itemize}
      \end{itemize}

    
  
  \subsection{Building indices}
    Once the files have been downloaded, it is time to build the indices over them.
    The program parses the original files to a CSV representation and, if needed (because the representation doesn't fit in memory as a python structure), transforms the CSV to a sqlite database. The process takes an hour or so in a modern computer. Since it is a sequential algorithm, speed depends on the hard drive I/O and the CPU capabilities (multicore is not used). Speed-ups can be achieved improving this components (i.e. using a SSD).
    
    \subsubsection{Pages}
      The file \texttt{enwiki-latest-pages-articles.xml} contains all the Wikipedia pages in a huge (~45GB) XML file. The data is parsed into a CSV file and then converted to a Sqlite3 file.\\
      
      The Sqlite3 file contains a table with one row per page with the following columns:
      \begin{itemize}
        \item \textbf{id}: numeric identifier of the page.
        \item \textbf{title}: title of the page
        \item \textbf{offset}: number of bytes from the beginning of the enwiki-latest-pages-articles.xml where the actual page can be found.
      \end{itemize}
      
      There are also built the following indices to speed up queries:
      \begin{itemize}
        \item \textbf{i\_id}: index over the id field.
        \item \textbf{i\_title}: index over the title field.
      \end{itemize}
      
      These are processing times:
      \begin{center}
      \begin{tabular}{c|c|c}
        \textbf{Source} & \textbf{Output} & \textbf{Time (min)}\\
        \hline
        xml\_pages & csv\_pages & 25 \\
        csv\_pages & sqlite\_pages & 5 \\
      \end{tabular}
      \end{center}


    \subsubsection{Categories}
      The file \texttt{enwiki-latest-category.sql} contains information about categories, but only the id and the title are extracted into a CSV file. As long as the CSV file can be loaded into a native Python structure, there is no need to have this information stored in an external database.

      This is the processing time:
      \begin{center}
      \begin{tabular}{c|c|c}
        \textbf{Source} & \textbf{Output} & \textbf{Time (min)}\\
        \hline
        sql\_category & csv\_category & 1\\
      \end{tabular}
      \end{center}


    
    \subsubsection{Categorylinks}
      The file \texttt{enwiki-latest-categorylinks.sql} contains the parent relationships between categories, and categories with pages. It is parsed to a CSV file  and then to a Sqlite3 file with the following columns:
      \begin{itemize}
        \item \textbf{child\_id}: id of the children page.
        \item \textbf{parent\_id}: id of the parent page.
        \item \textbf{type}: number that indicates the type of the children page.
          \begin{enumerate}
            \item page.
            \item category.
            \item file.
          \end{enumerate}
    \end{itemize}

    There are also built the following indices to speed up queries:
    \begin{enumerate}
      \item \textbf{i\_child}: index over the child\_id field.
      \item \textbf{i\_parent}: index over the parent\_id field.
    \end{enumerate}
    
    These are processing times:
      \begin{center}
      \begin{tabular}{c|c|c}
        \textbf{Source} & \textbf{Output} & \textbf{Time (min)}\\
        \hline
        sql\_categorylinks &  csv\_categorylinks &  15\\
        csv\_categorylinks &  sqlite\_categorylinks & 17\\
      \end{tabular}
      \end{center}

  
  \subsection{Parsing}
    For efficiency reasons, the original files are parsed with expat \cite{expat}, a Simple API for XML or SAX parser \cite{wiki_sax}. Contrary to Domain Object Model or DOM parsers who treat the file as a whole, SAX ones go across the file sequentially and only keeps in memory what is needed for the task. In this case the files can be seen as bunch of articles or relationships, not as trees or other more machine demanding structures. That is why expat has turned out to be the perfect parser for the indexing process.
    