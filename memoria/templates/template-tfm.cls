%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% AUTHOR: Samuel Mu�oz Hidalgo
% CONTACT: samuel.mh@gmail.com
% DESCRIPTION: Masters Thesis Template
%             Universidad Aut�noma de Madrid
% LAST REVISION: June 2014
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------ESTILOS DE P�GINA
% cuerpo:
% plain:
% chaptername:


%--------COMANDOS
%
% \citaAutor{cita}{autor} : Escribe la cita en cursiva y el autor en otra l�nea
% \nota{x} : Para hacer anotaciones en el texto
% \Anexo{x} : Empieza un cap�tulo de anexo
% \BASHfich{x} : Coge el fichero x y lo muestra como c�digo formateado




%--------ENTORNOS
%
% Ejemplo : poner un ejemplo largo 
% Problema : Para plantear un problema, tiene contador propio
% Respuesta : Para exponer una respuesta
% Consola : Para poner una traza de consola



\LoadClass[english,a4paper,twoside,openright]{book}



%-------- PACKAGES --------

\usepackage[utf8]{inputenc}
\usepackage[activeacute,english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[dvipsnames, usenames]{color}
  \definecolor{Gray}{rgb}{0.5,0.5,0.5}
\usepackage[pdftex]{hyperref}
  \hypersetup{
    colorlinks    = true, 
    urlcolor      = blue,
    citecolor     = Gray,
    pdfauthor     = {Samuel M.H. <samuel.mh@gmail.com>},
    pdftitle      = {},
    pdfsubject    = {},
    pdfkeywords   = {},
  % pdfstartview      = {FitH},
    bookmarksnumbered = {true}
  }


\usepackage{graphicx}
\usepackage{alltt}

\usepackage[all]{hypcap} %Para que los enlaces a figuras sean a la imagen, no al pie de imagen


\usepackage{ifthen}   %Para poner la 1� l�nea de consola en negrita o indentarla
\usepackage{fancyvrb} %Para la orden consola
\usepackage{listings} %Para c�digos fuente
\usepackage{umoline}  %Subrayado con salto de l�nea
\usepackage{fancyhdr} %Cabeceras y pies de p�gina
\usepackage{setspace} %Interlineados varios
\usepackage[square,sort,comma,numbers]{natbib}   %Bibliograf�a

\usepackage{eurosym}   %S�mbolo del euro
\usepackage{multicol}  %Varias columnas por p�gina
\usepackage{pdflscape}    %P�ginas horizontales
% \usepackage{frcursive} %Tipograf�a caligr�fica (agradecimientos)





%-------- MARGINS --------

% \oddsidemargin -.5cm
\evensidemargin -.3cm
\textwidth 15.5cm
% \textheight 25cm
% \topmargin 0cm
% \parindent .2cm
% \parskip 0ex
% \headheight 0.0cm
% \headsep 0cm
\marginparwidth 0cm


%-------- Page styles --------

% Para las p�ginas normales
\fancypagestyle{body}{
  \fancyhf{} %Borrar cabeceras y pies
  \fancyhead[LE,RO]{\slshape \rightmark}
  \fancyhead[LO,RE]{\slshape \leftmark}
  \fancyfoot[LE,RO]{\thepage}
  \fancyfoot[LO,RE]{Samuel M.H.}
  \renewcommand{\headrulewidth}{1pt}
  \renewcommand{\footrulewidth}{0.4pt}
}

% Redefino el estilo plain (primera p�gina de cap�tulos, partes e �ndice)
\fancypagestyle{plain}{%
  \fancyhf{} %Borrar cabeceras y pies
  \fancyfoot[LE,RO]{\thepage}
  \fancyfoot[LO,RE]{Samuel M.H.}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0.4pt}
}

% Pone en la cabecera el nombre del cap�tulo (bibliograf�a, �ndice, etc)
\fancypagestyle{chaptername}{
  \fancyhf{} %Borrar cabeceras y pies
  \fancyhead[LE,RO]{\slshape \leftmark}
  \fancyfoot[LE,RO]{\thepage}
  \fancyfoot[LO,RE]{Samuel M.H.}
  \renewcommand{\headrulewidth}{1pt}
  \renewcommand{\footrulewidth}{0.4pt}
}





%-------- COMMANDS --------

%citaAutor {cita}{autor}
%Escribe la cita en cursiva y el autor en otra l�nea
\newcommand{\citaAutor}[2]{
	\begin{large}
		\textit{#1}
	\end{large}
	\begin{flushright}
		\hfill{#2}\\[0.7cm]
	\end{flushright}	
}


%Nota {anotaci�n}
%Para hacer un p�rrafo de anotaci�n
\newcommand{\nota}[1]{
	\vspace{1eM}
	\paragraph{NOTA:}\textit{#1}
	\newline
}

%Apendices
%Crea la parte de apendices
\newcommand{\Apendices}{
	\phantomsection
	\addcontentsline{toc}{part}{Ap�ndices}
	\part*{Ap�ndices}
	\appendix
	\fancyhead[RE,LO]{}
}


%BASHfich {ruta}
%Coge un fichero bash y lo muestra
\newcommand{\BASHfich}[1]{
	\lstset{
		tabsize=2,
		language=bash,
		basicstyle=\fontfamily{cmtt}\small,
		keywordstyle=\fontfamily{cmss}\color{black}\bfseries,
		commentstyle=\fontfamily{cmss}\textcolor[rgb]{0.35,0.35,0.35},
		stringstyle=\textcolor[rgb]{1,0,0},
		showstringspaces=false,
		breaklines=true,
		numbers=left,
		numberstyle=\tiny,
		stepnumber=2,
		numbersep=.7cm
	}
	\lstinputlisting{#1}
}

%PHPfich {ruta}
%Coge un fichero PHP y lo muestra
\newcommand{\PHPfich}[1]{
	\lstset{
		tabsize=2,
		language=PHP,
		basicstyle=\fontfamily{cmtt}\footnotesize,
		keywordstyle=\fontfamily{cmss}\color{black}\bfseries,
		commentstyle=\fontfamily{cmss}\textcolor[rgb]{0.35,0.35,0.35},
		stringstyle=\textcolor[rgb]{1,0,0},
		showstringspaces=false,
		breaklines=true,
		numbers=left,
		numberstyle=\tiny,
		stepnumber=2,
		numbersep=.7cm
	}
	\lstinputlisting{#1}
}

%PERLfich {ruta}
%Coge un fichero Perl y lo muestra
\newcommand{\PERLfich}[1]{
	\lstset{
		tabsize=2,
		language=Perl,
		basicstyle=\fontfamily{cmtt}\footnotesize,
		keywordstyle=\fontfamily{cmss}\color{black}\bfseries,
		commentstyle=\fontfamily{cmss}\textcolor[rgb]{0.35,0.35,0.35},
		stringstyle=\textcolor[rgb]{1,0,0},
		showstringspaces=false,
		breaklines=true,
		numbers=left,
		numberstyle=\tiny,
		stepnumber=2,
		numbersep=.7cm
	}
	\lstinputlisting{#1}
}

%CONFfich {ruta}
%Coge un fichero Perl y lo muestra
\newcommand{\CONFfich}[1]{
	\lstset{
		tabsize=2,
		language=Clean,
		basicstyle=\fontfamily{cmtt}\footnotesize,
		keywordstyle=\fontfamily{cmss}\color{black}\bfseries,
		commentstyle=\fontfamily{cmss}\textcolor[rgb]{0.35,0.35,0.35},
		stringstyle=\textcolor[rgb]{1,0,0},
		showstringspaces=false,
		breaklines=true,
		numbers=left,
		numberstyle=\tiny,
		stepnumber=2,
		numbersep=.7cm
	}
	\lstinputlisting{#1}
}

%SQLfich {ruta}
%Coge un fichero SQL y lo muestra
\newcommand{\SQLfich}[1]{
	\lstset{
		tabsize=2,
		language=SQL,
		basicstyle=\small,
		keywordstyle=\color{black}\bfseries,
		commentstyle=\color{blue},
		stringstyle=\ttfamily,
		showstringspaces=false,
		breaklines=true
	}
	\lstinputlisting{#1}
}



%-------- ENTORNOS --------

%Ejemplo
\newcounter{CEjemplo}
\setcounter{CEjemplo}{0}
\newenvironment{Ejemplo}{
	\stepcounter{CEjemplo}
	\par
	\textbf{\textsf{\textcolor[rgb]{0.2,0.2,0.2}{Ejemplo(\arabic{CEjemplo}) }}}
	\begin{small}\begin{sffamily}
}
{
	\end{sffamily}\end{small}
	\vspace{1.5eM}
}

%Problema (tiene contador de problemas)
\newcounter{CProb}
\setcounter{CProb}{0}
\newenvironment{Problema}
	{\stepcounter{CProb}\vspace{3eM}\textbf{\textsf{\textcolor[rgb]{0.9,0,0}{PROBLEMA \arabic{CProb}}}}\\}
	{\par}
	
%Respuesta
\newenvironment{Respuesta}
	{\vspace{1eM}\textbf{\textsf{\textcolor[rgb]{0,0.7,0}{\indent RESPUESTA}}}\\}
	{\par}


%Consola
%Hacer que el fancyvbr parta las l�neas largas
\renewcommand{\FancyVerbFormatLine}[1]{
		\begin{minipage}{\the\textwidth}
			\textcolor{black}{
			\begin{small}
				#1 %Primera y resto de l�neas
			\end{small}
			}
    \end{minipage}
}
\DefineVerbatimEnvironment{Consola}{Verbatim}
	{frame=single,framesep=5mm,numbers=left,label=consola}



%Redefiniendo el entorno Verbatim para que formatee c�digo tabulado
\RecustomVerbatimEnvironment{Verbatim}{Verbatim}
	{tabsize=3, numbers=left, framesep=5mm}


% Chapter: {path}
\newcommand{\smhCurrentDir}{}
\newcommand{\smhChapter}[1]{
  \renewcommand{\smhCurrentDir}{chapters/#1/}
  \graphicspath{{\smhCurrentDir/images/}}
  \include{\smhCurrentDir/#1}
}

\newcommand{\smhInput}[1]{
  \input{\smhCurrentDir/#1}
}
	

%
%%% COMMANDS INHERITED FROM TFM PROJECTS
%


% Image: [size]{caption}{file}{label}
\newcommand{\smhImg}[4][12cm]{
  \begin{figure}[ht!]
    \begin{center}
      \includegraphics[width=#1]{#3}
      \caption{#2}
      \label{#4}
    \end{center}
  \end{figure}
}


% Text: [text size]{caption}{file}{label}
\newcommand{\smhText}[4][normalsize]{
  \begin{figure}[ht!]
    \begin{center}
      \begin{#1}
        \verbatiminput{#3}
      \end{#1}
      \caption{#2}
      \label{#4}
    \end{center}
  \end{figure}
}


% Source code: [options]{caption}{file}{label}
\newcommand{\smhCode}[4][]{
  \begin{figure}[ht!]
    \lstinputlisting[#1]{#3}
    \caption{#2}
    \label{#4}
  \end{figure}
}


% Table: {caption}{file}{label}
\newcommand{\smhTable}[3]{
  \begin{table}[ht!]
    \begin{center}
      \smhInput{tables/#2}
      \caption{#1}
      \label{#3}
    \end{center}
  \end{table}
}




%
%%% FONT ENVIRONMENTS
%
\newenvironment{smhTexttt}{\noindent \ttfamily \begin{center}}{\\ \end{center} \par}
\newenvironment{smhEmph}{\noindent \itshape \begin{center}}{\\ \end{center} \par}